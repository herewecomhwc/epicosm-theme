<div class="container">
    <?php
    $order = 'normal';
    if(get_field('reverse_order')):
        $order = 'reverse';
    endif;
    ?>
    <div class="order <?=$order;?>">
        <?php
        $blocks = get_field('blocks');
        $delay = 1.5;
        foreach ($blocks as $line):
            $multiple = false;
            $size = 'rectangle_nc';
            if(count($line['images']) > 1):
                $multiple = true;
                $size = 'square';
            endif;
        ?>
        	<article class="<?php if($line['images'][0]['width'] > $line['images'][0]['height']): echo "rectangle"; else: echo "portrait"; endif; ?>">
        		<div class="images<?php if($multiple): ?> multiple<?php endif; ?>">
        			<?php
        			foreach($line['images'] as $image): ?>
        				<?php 
        				$image_data = getImage($size.'_@1x', $image['ID'], array('alt' => $image['alt']), 'full');
        				?>
                		<div class="image ir ir-hover <?php if($multiple): ?>ir-square<?php else:?>ir-rectangle<?php endif; ?>">
                			<a class="popin" href="<?=$image_data['url'];?>">
            	    			<?=$image_data['html'];?>
            	    			<?=$image_data['html'];?>
                			</a>
                		</div>
            		<?php endforeach; ?>
        		</div>
        		<div class="text editorial animate to-right fade <?php animationDisplayDelay($delay); ?>">
        			<?php if($line['floated_img']): ?>
        				<?=getImage('rectangle_nc_@0.5x', $line['floated_img']['ID'], array('class' => 'floated'))?>
        			<?php endif; ?>
        			<?= $line['text']; ?>
        		</div>
        	</article>
        <?php 
        animationIncreaseDelay($delay);        
        endforeach; ?>
    </div>
</div>
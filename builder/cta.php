<?php
$bg = get_field('bg');
if(!$bg):
    $bg = array('ID' => false);
endif;
$image = get_field('image');
$content = get_field('content');
?>
<div class="bg-container">
	<div class="container">
    	<?php if($image): ?>
        	<div class="img animate to-top fade">
        		<?=getImage('rectangle_@1x', $image['ID'], array('class' => 'animate to-top fade'));?>
        	</div>
    	<?php endif; ?>
    	<div class="content animate to-bottom fade">
			<?=getImage('rectangle_@1x', $bg['ID'], array('class' => 'bg'));?>
			<div class="content-container"><?=$content;?></div>
		</div>
	</div>
</div>
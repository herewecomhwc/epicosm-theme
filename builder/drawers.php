<div class="container">
	<?php
	include('inc/block-title.php');
	$drawers = get_field('drawers');
	?>
	<div class="items">
    	<?php 
    	foreach ($drawers as $line): ?>
    		<div class="item">
    			<div class="head">
    				<h3><?= $line['title']; ?></h3>
    				<div class="toggle"><span class="minus"><?php displaySvg('minus.svg');?></span><span class="plus"><?=displaySvg('plus.svg'); ?></span></div>
    			</div>
    			<div class="content"><?= $line['text']; ?></div>
    		</div>
    	<?php endforeach; ?>
	</div>
</div>

<?php
$text = get_field('text');
$img = get_field('img');
$values = get_field('values');
?>
<div class="text animate fade to-bottom"><?=$text;?></div>
<div class="img animate fade delay 1-5s"><?= getImage('rectangle_nc_@1x', $img['ID']); ?></div>
<div class="values">
	<?php 
	$delay = 1.75;
	foreach($values as $value): ?>
		<div class="value animate fade to-right <?php animationDisplayDelay($delay); ?>">
			<div class="icon"><?=getImage('rectangle_nc_@0.5x', $value['icon']['ID']); ?></div>
			<div class="label"><?=$value['value'];?></div>
		</div>
	<?php 
	animationIncreaseDelay($delay);
	endforeach; ?>
</div>
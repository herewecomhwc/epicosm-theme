<?php 
$only_form = get_field('only_form');

?>
<div class="container<?php if($only_form):?> only_form<?php endif; ?>">
	<?php if(!$only_form): ?><div class="map-container"><?= do_shortcode('[hwcgmap acf_id="contacts" div_id="map_societe" acf_page_id="config-contact" marker_img="marker-gmap@2x.png"]'); ?></div><?php endif; ?>
	<div class="right">
		<?php if(!$only_form): ?>
        	<div class="contacts-block">
        		<div class="contacts">
        			<?php
        			$contacts = get_field('contacts');
        			foreach ($contacts as $contact): ?>
        			<div class="contact">
        				<?php
        				if($contact['default_contact']):
            				$localisation = do_shortcode("[hwccontact field='address']").'<br/>'.do_shortcode("[hwccontact field='postal_code']").' '.do_shortcode("[hwccontact field='city']").' '.do_shortcode("[hwccontact field='country']");
            				$phone = '<span>'.do_shortcode("[hwccontact field='phone' linked=1]").'</span><span>'.do_shortcode("[hwccontact field='mobile' linked=1]").'</span><span>'.do_shortcode("[hwccontact field='fax' linked=1]").'</span>';
            				$email = do_shortcode("[hwccontact field='e-mail' linked=1]");
        				else:
        				    $localisation = $contact['address'];
        				    $contact['phone'] = displayEmail($contact['phone'], false);
        				    $phone = "<a class='email-js' href='tel:".$contact['phone']."'>".$contact['phone']."</a>";
        				    if($contact['mail']):
            				    $contact['mail'] = displayEmail($contact['mail'], false);
            				    $email = "<a class='email-js' href='mailto:".$contact['mail']."'>".$contact['mail']."</a>";
        				    else:
        				        $email = '';
        				    endif;
        				endif;
        				?>
        				<?php if(!empty($localisation)): ?>
        					<div class="localisation info">
        	    				<span class="icon"><i class="fa fa-map-marker-alt" aria-hidden="true"></i></span>
        	    				<span class="right">
            	    				<span class="title"><?=__('Adresse', 'wpb');?></span>
            	    				<span class="text"><?= $localisation; ?></span>
        	    				</span>
        
        					</div>
        				<?php endif; ?>
        				<?php if(!empty($phone)): ?>
        					<div class="phone info">
        						<span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
        						<span class="right">
            						<span class="title"><?=__('Téléphone', 'wpb');?></span>
            						<span class="text"><?= $phone; ?></span>
        						</span>
        					</div>
        				<?php endif; ?>
        				<?php if(!empty($email)): ?>
            				<div class="mail info">
            					<span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
            					<span class="right">
                					<span class="title"><?=__('Mail', 'wpb');?></span>
            						<span class="text"><?=$email;?></span>
        						</span>
            				</div>
        				<?php endif; ?>
        			</div>
        			<?php endforeach; ?>
        		</div>
        	</div>
    	<?php endif; ?>
    	<div class="form">
    		<h2><?=__('Contactez-nous', 'wpb');?></h2>
        	<?= do_shortcode('[contact-form-7 id="'.get_field('form').'"]'); ?>
    	</div>
	</div>
</div>
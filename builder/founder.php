<?php
$photo = get_field('photo');
$photo_mobile = get_field('photo_mobile');
if(!$photo_mobile):
$photo_mobile = $photo;
endif;
$block_title = get_field('block_title');
$description = get_field('description');
$firstname_lastname = get_field('firstname_lastname');
$blockquote = get_field('blockquote');
?>
<div class="container">
	<div class="left animate to-right fade">
		<?=getImage('portait_@1x', $photo['ID']); ?>
		<?=getImage('portait_@1x', $photo_mobile['ID']); ?>
	</div>
	<div class="right">
		<h2 class="animate to-bottom fade delay-1-5s"><?=$block_title;?></h2>
		<div class="text animate to-bottom fade delay-1-5s"><?=$description;?></div>
		<div class="author animate to-right fade delay-1-75s"><?=$firstname_lastname; ?></div>
		<blockquote class="animate to-bottom fade delay-2s"><?=$blockquote;?></blockquote>
	</div>
</div>
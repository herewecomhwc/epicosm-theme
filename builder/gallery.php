<div class="container">
    <?php
    $i=1;
    $gal_id = uniqid();
    $images = get_field('images');
    foreach ($images as $galimage): ?>
    	<?php if($i == 1): ?><div class="left"><?php endif; ?>
    	<?php if($i == 4): ?></div><div class="right"><?php endif; ?>
    	<div class="image ir ir-hover ir-rectangle">
    		<?php $image_data = getImage('rectangle_nc', $galimage['ID'], array('alt' => $galimage['alt']), 'full');?>
    		<a class="popin" rel="gal-<?= $gal_id; ?>" href="<?= $image_data['url']; ?>">
    			<?=$image_data['html'];?>
    		</a>
    	</div>
    <?php
    $i++;
    endforeach; ?>
    </div>
</div>
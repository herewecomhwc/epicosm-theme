<?php
$image = get_field('image');
if(get_field('parallax')): ?>
	<div class="parallax-window" data-parallax="scroll" data-speed="0.6" data-image-src="<?= getImage('rectangle_large', $image['ID'], array(), 'url'); ?>"></div>
<?php else: ?>
	<?=getImage('rectangle_large', $image['ID'], array('alt' => $image['alt'])); ?>
<?php endif; ?>
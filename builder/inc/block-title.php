<?php
$title = get_field('block_title');
$description = get_field('block_description');
if($title || $description):
?>
    <div class="block-title">
    <?php
        if($title):
        ?>
        	<h2 class="animate fade to-right"><?=$title;?></h2>
        <?php
        endif;
        if($description):
        ?>
        	<div class="description animate fade to-left">
        		<?=$description;?>
        	</div>
        <?php
        endif;
        ?>
    </div>
<?php
endif;
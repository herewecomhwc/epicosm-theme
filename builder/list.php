<?php
    $blocks_display = get_field('display_type');
    $images_display = get_field('images_display');
    $column_count = get_field('column_count');
    if(!$images_display): $images_display = 'classic'; endif;
    if(!$column_count): $column_count = '3'; endif;
?>
<div class="container">
	<?php include('inc/block-title.php');
	$list = get_field('list');
	if($list):
	   $delay = 1.5;
	?>
        <div class="list-container blocks-display-<?=$blocks_display;?> images-display-<?=$images_display;?> column-count-<?=$column_count;?>">
        	<?php foreach($list as $item): ?>
                <div class="item animate fade to-right <?php animationDisplayDelay($delay); ?>">
                	<?php
                	   $image = $item['image'];
                	   if($image):
                	       $image = getImage('rectangle_@1x', $image['ID']);
                	   endif;
                	   $label = $item['label'];
                	   $btn = $item['btn'];
                	   $title = $item['title'];
                	   $description = $item['description'];
                	   if($btn && $image):
                	       $image = "<a class='ir ir-hover ir-rectangle' href='".$btn['url']."' target='".$btn['target']."'>".$image."</a>";
                	   endif;
                	   if($btn && $title):
                	       $title = "<a href='".$btn['url']."' target='".$btn['target']."'>".$title."</a>";
                	   endif;
                	?>
                	<?php if($image || $label): ?>
                    	<div class="image">
                    		<?=$image;?>
                    		<?php if($label): ?>
                        		<div class="label">
                            		<?= $label ?>
                            	</div>
                        	<?php endif; ?>
                    	</div>
                	<?php
                	endif;
                    if($title || $description || $btn):
                	?>
                	<div class="text">
                 		<?php if($title): ?><h3><?=$title; ?></h3><?php endif; ?>
                		<?php if($description): ?><div class="description"><?=$description; ?></div><?php endif; ?>
                		<?php if($btn): ?><a href="<?=$btn['url'] ?>" target="<?=$btn['target'] ?>" class="btn link"><?=$btn['title']. displaySvg('arrow-right-4.svg', false); ?></a><?php endif; ?>
                	</div>
                	<?php endif; ?>
                </div>
            <?php 
            animationIncreaseDelay($delay);
            endforeach; ?>
        </div>
    <?php
    endif;
    $btn = get_field('btn');
    if($btn):
    ?>
        <div class="btn-container animate to-top fade <?php animationDisplayDelay($delay+0.25); ?>">
        	<a href="<?=$btn['url'];?>" target="<?=$btn['target'];?>" class="btn default"><?=$btn['title'].displaySvg('arrow-right-4.svg', false);?></a>
        </div>
    <?php
    endif;
    ?>
</div>
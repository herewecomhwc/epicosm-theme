<?php
global $blogpage_id;
if(!isset($nb_posts)):
    $nb_posts = get_field('nb_posts');
endif;
?>
<div class="container">
	<?php
    $title = get_field('block_title');
    $description = get_field('block_description');
    $btn_label = get_field('btn_label');
    if($title || $description):
    ?>
        <div class="block-title">
        <?php
            if($title):
            ?>
            	<h2 class="animate fade to-right h1"><?=$title;?></h2>
            <?php
            endif;
            if($description || $btn_label):
            ?>
            	<div class="description animate fade to-left">
            		<?=$description;?>
            		<?php if($btn_label): ?>
                        <div class="btn-container animate fade to-top <?php animationDisplayDelay(1.5); ?>">
                        	<a href="<?=get_permalink($blogpage_id)?>" class="btn link"><?=$btn_label;?></a>
                        </div>
                    <?php endif; ?>
            	</div>
            <?php
            endif;
            ?>
        </div>
    <?php
    endif;
	 ?>
    <?php
    	$myposts = new WP_Query(array(
    	    'lang' => pll_current_language(),
    	    'posts_per_page' => $nb_posts,
    	    'post_status' => 'publish',
    	    'post_type' => 'post',
    	    'post__not_in' => array(get_the_ID()))
    	);
    	if($myposts->have_posts()): ?>
    	<div class="
    	<?php if(is_single()): ?>
    		list-container blocks-display-column images-display-classic column-count-4
    	<?php else: ?>
    		list-container blocks-display-column images-display-classic <?php if($myposts->post_count >= 4): ?>column-count-4<?php else: ?>column-count-<?=$myposts->post_count;?><?php endif; ?>
    	<?php endif;?>
    	">
        	<?php
        	   $delay = 1.5;
            	while ( $myposts->have_posts() ) : $myposts->the_post();
            	   get_template_part( 'item-list', 'news', array('delay' => $delay) );
            	   animationIncreaseDelay($delay);
            	endwhile;
            	wp_reset_postdata();
        	?>
        </div>
        <?php
    	endif;
        ?>
</div>
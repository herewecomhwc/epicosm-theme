<?php
$numbers = get_field('numbers');
if($numbers): ?>
    <div class="container numbers">
    	<?php foreach ($numbers as $line): ?>
    	<div class="item">
    		<div class="line line-1"><?= $line['value_1']; ?></div>
    		<div class="line line-2"><?= $line['value_2']; ?></div>
    	</div>
    	<?php endforeach; ?>
    </div>
<?php endif; ?>
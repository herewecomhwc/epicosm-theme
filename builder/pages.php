<?php
$pages = get_field('pages');
?>
<div class="container">
	<?php foreach($pages as $page): ?>
	<div class="page">
		<a href="<?=get_the_permalink($page['page']); ?>"></a>
		<?=getImage('rectangle_@1x', $page['image']['ID']); ?>
		<div class="text">
			<h2><?=get_the_title($page['page']); ?></h2>
			<a href="<?=get_the_permalink($page['page']); ?>" class="btn link"><?=__('En savoir plus', 'wpb');?></a>
		</div>
	</div>
	<?php endforeach;?>
</div>
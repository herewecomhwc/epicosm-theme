<?php $cid = get_cpt_homepage_id('partner'); ?>
<div class="page-item filters">
	<div class="container">
		<a href="<?php echo get_permalink($cid); ?>" class="btn link animate to-bottom fade <?php if(get_the_ID() == $cid): ?>current<?php endif;?>"><?php _e('All', 'wpb'); ?></a>
		<?php
            $taxonomy = 'partner-cat';
            $taxonomy_terms = get_terms($taxonomy, array(
                'hide_empty' => 0,
                'orderby' => 'term_order',
            ));
            if($taxonomy_terms):
                $delay = 1.5;
                foreach ($taxonomy_terms as $pcat) :
                ?>
        		<a href="<?php echo get_term_link($pcat->term_id); ?>" class="btn link animate to-bottom fade <?php animationDisplayDelay($delay); ?> <?php if(get_queried_object()->term_id == $pcat->term_id): ?>current<?php endif;?>"><?php echo $pcat->name; ?></a>
        		<?php 
        		animationIncreaseDelay($delay);
	           endforeach;
	       endif;
		?>
	</div>
</div>
<div id="ajax-target" class="container list-container blocks-display-column images-display-classic column-count-3 animate-childrens">
	<?php
        if (get_query_var('paged')):
            $paged = get_query_var('paged');
        elseif (get_query_var('page')):
            $paged = get_query_var('page');
        else:
            $paged = 1;
        endif;
        $args = array(
            'post_type' => 'partner',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged,
            'lang' => pll_current_language()
        );
        if (is_tax('partner-cat')) :
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'partner-cat',
                    'terms' => get_queried_object()->term_id,
                    'field' => 'term_id'
                )
            );
        endif;
        $myposts = new WP_Query($args);
        if ($myposts->have_posts()) :
            $delay = 1.5;
            while ($myposts->have_posts()) :
                $myposts->the_post();
                get_template_part('item-list', 'partners', array('delay' => $delay));
                animationIncreaseDelay($delay);
            endwhile;
            wp_reset_postdata(); 
        endif;
        ?>
</div>

<div class="page-item paginate infinite"><?php displayPagination($myposts, $args, 'partners'); ?></div>
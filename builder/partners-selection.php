<div class="bg-color">
    <?php
    global $post;
    $unique_id = md5(uniqid(rand(), true));
    $myposts = get_field('partners');
    if($myposts): ?>
    	<div class="container">
    		<?php
        	$inside_title = get_field('block_title');
        	if($inside_title):
        	?>
            	<h2><?=$inside_title;?></h2>
        	<?php
        	endif;
        	?>
    	</div>
        <div class="container">
        	<div class="owl-carousel owl-partners" id="<?=$unique_id;?>">
            	<?php foreach ( $myposts as $post): setup_postdata($post);  ?>
            	   <div class="item">
                	   	<?php if(get_field('website', get_the_ID())): ?>
        	    			<a href="<?= addScheme(get_field('website', get_the_ID())); ?>" target="_blank" rel="noopener">
        	    		<?php
        	    		endif;
        	    		the_post_thumbnail('rectangle_nc');
        	    		if(get_field('website', get_the_ID())): ?>
        	    			</a>
        	    		<?php endif; ?>
            	   </div>
            	   <?php
            	endforeach;
            	wp_reset_postdata(); ?>
            </div>
            <div class="slide-custom-nav">
            	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]'><?php displaySvg('arrow-left-3.svg');?></span>
            	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]'><?php displaySvg('arrow-right-3.svg');?></span>
            </div>
        </div>
    <?php
    endif;
    ?>
</div>
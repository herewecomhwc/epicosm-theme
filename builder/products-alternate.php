<?php
global $homepage_id;
$products = get_field('products');
if($products):
    foreach($products as $product_id):
    ?>
		<div class="item">
			<div class="image">
				<?=getImage('portrait_@1x', get_post_thumbnail_id($product_id)); ?>
			</div>
			<div class="infos">
				<div class="content">
					<h2><?=get_the_title($product_id); ?></h2>
					<div class="datas">
						<span class="price"><?php 
						$wc_product = wc_get_product($product_id); 
						echo $wc_product->get_price_html();?></span>
						<span class="info"><?=get_field('main_product_attribute', $product_id);?></span>
						<span class="product-score"><?php displayProductScore($product_id); ?></span>
					</div>
					<div class="description"><?=get_field('description_short', $product_id);?></div>
					<div class="buttons">
						<a href="<?=get_permalink($product_id);?>" class="btn default"><?=__('En savoir plus', 'wpb');?></a>
						<a href="<?=get_permalink($product_id);?>?add-to-cart=<?=$product_id;?>&quantity=1" class="btn default"><?=__('Acheter', 'wpb');?></a>
					</div>
				</div>
			</div>
		</div>    
    <?php 
    endforeach;
endif;
?>

<?php
$block_title = get_field('block_title');
$pages = get_field('pages');
$link = get_field('link');
$unique_id = md5(uniqid(rand(), true));
$bg = get_field('bg');
?>
<div class="bg<?php if(!$bg): ?> empty<?php endif; ?>"><?php if($bg): echo getImage('rectangle_nc_@1x', $bg['ID']); endif; ?></div>
<div class="container">
	<div class="left">
		<h2><?=$block_title;?></h2>
		<?php if($link): ?><a href="<?=$link['url'];?>" target="<?=$link['target'];?>" class="btn link"><?=$link['title'];?></a><?php endif; ?>
		<div class="slide-custom-nav<?php if(count($pages) <= 3):?> no-nav-desktop<?php endif; ?>">
        	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]'><?php displaySvg('slider-left.svg');?></span>
        	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]'><?php displaySvg('slider-right.svg');?></span>
        </div>
	</div>
	<div class="items" id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"true"}, {"mouseDrag":"false"}, {"nav":"false"}, {"items":"3"}, {"margin":"38"}<?php if(count($pages) <= 3):?>, {"autoplay":"false"}<?php endif; ?>,{"responsive":{"1265" : {"items":"3"}, "965" : {"items":"2"}, "768" : {"items":"1"}, "575" : {"items":"2"}, "0" : {"items":"1"}}}]'>
		<?php 
		if($pages): 
		  foreach($pages as $page):
		      
		?>
		<div class="item">
			<a href="<?=get_permalink($page['post']);?>"><?= getImage('square_@1x',$page['image']); ?></a>
			<div class="title"><a href="<?=get_permalink($page['post']);?>"><?=get_the_title($page['post']);?></a></div>
		</div>
		<?php 
            endforeach;		
		endif ?>
	</div>
</div>
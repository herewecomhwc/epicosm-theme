<?php
$block_title = get_field('block_title');
$block_description = get_field('block_description');
$btn = get_field('btn');
$products = get_field('products');
$quote = get_field('quote');
$unique_id = md5(uniqid(rand(), true));
?>
<div class="container">
	<div class="left">
		<div class="block-title">
            <h2 class="animate fade to-right"><?=$block_title;?></h2>
            <div class="description animate fade to-left">
            	<?php if($block_description): echo '<p>'.$block_description.'</p>'; endif; ?>
        		<a href="<?=$btn['url'];?>" class="btn link"><?=$btn['title'];?></a>
        	</div>
    	</div>
	</div>
	<div class="items" id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"true"}, {"mouseDrag":"false"}, {"nav":"false"}, {"items":"2"}, {"autoplay":"false"}, {"responsive":{"900" : {"items":"2"}, "0" : {"items":"1"}}}]'>
		<?php 
		if($products):
            foreach($products as $product):
                $wc_product = wc_get_product($product->ID);
                ?>
            	<div class="product">
            		
            		<div class="image ir ir-hover ir-3-2">
            			<a href="<?=get_permalink($product->ID);?>">
            				<?=getImage('portrait_@1x', get_post_thumbnail_id($product->ID)); ?>
            			</a>
            		</div>
            		
            		<div class="categories"><?php $terms = get_the_terms( $product->ID, 'product_cat' ); 
            		if($terms):
            		  $names = array();
            		  foreach($terms as $term):
            		      $names[] = $term->name;
            		  endforeach;
            		  echo implode(', ', $names);
            		endif;
            		?></div>
            		<div class="text">
            			<h2 class="title"><a href="<?=get_permalink($product->ID);?>"><?=get_the_title($product->ID); ?></a></h2>
            			<div class="price"><?=$wc_product->get_price_html();?></div>
            		</div>
            	</div>
            <?php 
            endforeach;
        endif;
        ?>
	</div>
	<div class="right">
    	<blockquote><?php displaySvg('quote-banner.svg'); ?><?=$quote;?></blockquote>
	</div>
	<div class="slide-custom-nav<?php if(count($products) < 3):?> no-nav-desktop<?php endif; ?>">
    	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]' data-onclick='[{"class":"to-left","action":"addClass","target":"#<?=$unique_id;?> .owl-item"}, {"class":"to-right","action":"removeClass","target":"#<?=$unique_id;?> .owl-item"}]'><?php displaySvg('slider-left.svg');?></span>
    	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]' data-onclick='[{"class":"to-left","action":"removeClass","target":"#<?=$unique_id;?> .owl-item"}, {"class":"to-right","action":"addClass","target":"#<?=$unique_id;?> .owl-item"}]'><?php displaySvg('slider-right.svg');?></span>
    </div>
</div>
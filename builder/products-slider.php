<?php
$block_title = get_field('block_title');
$link = get_field('link');
$products = get_field('products');
?>
<div class="container">
	<?php if($block_title || $link): ?>
	<div class="block-title">
        <?php if($block_title): ?><h2 class="animate fade to-right h1"><?=$block_title;?></h2><?php endif; ?>
        <?php if($link): ?>
            <div class="description animate fade to-left">
        		<a href="<?=$link['url'];?>" class="btn link"><?=$link['title'];?></a>
        	</div>
    	<?php endif; ?>
    </div>
    <?php endif; ?>
    <?php if($products): 
        $unique_id = md5(uniqid(rand(), true));
    ?>
    	<div class="slider-container">
            <div class="products-list" id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"true"}, {"mouseDrag":"false"}, {"nav":"false"}, {"items":"3"}, {"margin":"100"}, {"responsive":{"1024" : {"items":"3"}, "768" : {"items":"2"}, "0" : {"items":"1"}}}]'>
            	<?php foreach($products as $product): 
            	   $wc_product = wc_get_product($product->ID);
            	   $description_short = get_field('description_short', $product->ID);
            	   $description_simple = get_field('description_simple', $product->ID);
            	   if($description_simple):
            	       $description_short = $description_simple;
            	   endif;
            	   ?>
                	<div class="product">
                		<div class="image ir ir-hover ir-3-2">
                			<a href="<?=get_permalink($product->ID);?>">
                				<?=getImage('portrait_@1x', get_post_thumbnail_id($product->ID)); ?>
                			</a>
                		</div>
                		<div class="text">
                			<h2 class="title"><a href="<?=get_permalink($product->ID);?>"><?=get_the_title($product->ID); ?></a></h2>
                			<div class="price"><?=$wc_product->get_price_html();?></div>
                			<?php if($description_short):?><div class="description-short"><?=$description_short;?></div><?php endif; ?>
                		</div>
                	</div>
            	<?php endforeach;?>
            </div>
        	<div class="slide-custom-nav<?php if(count($products) <= 3):?> no-nav-desktop<?php endif; ?>">
            	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]'><?php displaySvg('slider-left.svg');?></span>
            	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]'><?php displaySvg('slider-right.svg');?></span>
            </div>
        </div>
    <?php endif; ?>
</div>
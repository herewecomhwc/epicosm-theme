<div class="container" data-slider-init='[{"dots":"false"}, {"loop":"false"}, {"mouseDrag":"true"}, {"nav":"false"}, {"items":"6"}, {"autoplay":"true"}, {"responsive":{"1275" : {"items":"6"}, "1050" : {"items":"5"}, "850" : {"items":"4"}, "650" : {"items":"3"}, "550" : {"items":"2"}, "0" : {"items":"1"}}}]'>
    <?php
    $elements = get_field('elements');
    if($elements):
        foreach($elements as $element):
        ?>
        <div class="el">
        	<?php if($element['icon']['mime_type'] == 'image/svg+xml'): ?>
        	<?php displaySvg($element['icon']['url'], true); ?>
        	<?php else: ?>
        	<?=getImage('rectangle_nc_@0.5x', $element['icon']['ID']); ?>
        	<?php endif; ?>
        	<div class="text"><?=$element['text'];?></div>
        </div>
        <?php           
        endforeach;
    endif;
    ?>
</div>
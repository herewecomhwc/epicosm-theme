<div class="container">
	<?php 
    	$socials_share_enabled = get_field('authorized_social_networks', 'config-posts');
    	if(isset($socials_share_enabled[0])): // Si on en a au moins d'activé
        	?><span class="text"><?=get_field('label_post_social', 'config-content-'.pll_current_language());?></span><span class="line"></span><span class="icons"><?php
        	foreach ($socials_share_enabled as $social):
        	    switch ($social):
        	        case 'fb' : // Facebook
        	            ?>
        				<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(get_the_ID()); ?>" target="_blank" rel="noopener" title="<?php _e('Share on Facebook !', 'wpb'); ?>"><i class="fab fa-facebook-f"></i></a>
        				<?php
        				break ;
        			case 'tw' : // Twitter
        				?>
        				<a href="https://www.twitter.com/share?url=<?= get_the_permalink(get_the_ID()); ?>" target="_blank" rel="noopener" title="<?php _e('Share on Twitter !', 'wpb'); ?>"><i class="fab fa-twitter"></i></a>
        				<?php
        				break ;
        			case 'lk' : // Linkedin
        				?>
        				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(get_the_ID()); ?>&title=<?= get_the_title(get_the_ID()); ?>" target="_blank" rel="noopener" title="<?php _e('Share on Linkedin !', 'wpb'); ?>"><i class="fab fa-linkedin-in"></i></a>
        				<?php
        				break ;
        		endswitch;
        	endforeach;
        	?></span><?php // .icons
    	endif;
	?>
</div>
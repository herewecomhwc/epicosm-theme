<?php
$slider = get_field('slides');
$top_text = get_field('top_text');
$get_last_post = get_field('get_last_post');
$featured_post = get_field('featured_post');
$reassurances = get_field('reassurances');
$float_btn = get_field('float_btn');
$unique_id = md5(uniqid(rand(), true));
?>
<?php if($top_text): ?>
<div class="top"><?=$top_text;?></div>
<?php endif;
if($slider): ?>
	<div id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"true"}, {"autoplay":"true"}, {"autoplayHoverPause":"false"},{"mouseDrag":"false"}, {"nav":"false"}, {"animateIn": "fadeIn"}, {"animateOut": "fadeOut"}]'>
		<?php
		$h1_set = false;
		foreach ($slider as $slide): ?>
			<div class="item">
				<?php if($slide['slide_img']): ?>
					<?=getImage('rectangle', $slide['slide_img']['ID'], array('class' => 'bg', 'alt' => $slide['slide_img']['alt']));?>
				<?php endif; ?>
				<div class="content">
					<div class="title">
						<?php if(!$h1_set && is_front_page()):?>
							<h1>
						<?php endif; ?>
							<?= $slide['slide_title']; ?>
						<?php if(!$h1_set && is_front_page()):?>
							</h1>
						<?php 
						$h1_set = true;
						endif; ?>
					</div>
					<?php if($slide['slide_text']): ?>
					<div class="text"><?=$slide['slide_text'];?></div>
					<?php endif; ?>
					<?php if(isset($slide['slide_btn']['url'])): ?>
						<div class="btn-wrapper">
							<a href="<?= $slide['slide_btn']['url']; ?>" class="btn default" target="<?= $slide['slide_btn']['target']; ?>">
								<?= $slide['slide_btn']['title']; ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<?php for ($i = 1; $i <= 100; $i++):
                    ?><div class="circle-container"><div class="circle"></div></div><?php 
                endfor;?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
<div class="bottom">
	<div class="news"><?php 
    	if($get_last_post):
    	   $last_post = wp_get_recent_posts(array('posts_per_page' => 1));
    	   if(isset($last_post[0]['ID'])):
    	       $last_post_id = $last_post[0]['ID'];
    	   else:
    	       $last_post_id = false;
    	   endif;
    	else:
    	   $last_post_id = $featured_post;
    	endif;
    	?>
    	<div class="title"><?=get_the_title($last_post_id); ?></div>
    	<a href="<?=get_permalink($last_post_id); ?>" class="btn link"><?=__('Lire l\'article', 'wpb'); ?></a>
	</div>
	<div class="reassurances animate-childrens" data-slider-init='[{"dots":"false"}, {"loop":"false"}, {"mouseDrag":"true"}, {"nav":"false"}, {"items":"6"}, {"autoplay":"true"}, {"responsive":{"1275" : {"items":"6"}, "1050" : {"items":"5"}, "950" : {"items":"4"}, "850" : {"items":"3"}, "750" : {"items":"2"}, "0" : {"items":"1"}}}]'>
		<?php if($reassurances):  
		$delay = 1.5;
		foreach($reassurances as $reassurance): ?>
		<div class="ra-item animate fade to-top <?php animationDisplayDelay($delay); ?>">
			<div class="icon"><?php displaySvg($reassurance['icon']['url'], true); ?></div>
			<div class="text"><?=$reassurance['text'];?></div>
		</div>
		<?php 
		animationIncreaseDelay($delay);
		endforeach; endif; ?>
	</div>
</div>
<?php 
$float_btn = get_field('dealer_button', 'config-content-'.pll_current_language());
if($float_btn): ?>
<a href="<?=$float_btn['url'];?>" class="btn default vertical" target="<?=$float_btn['target'];?>"><?=$float_btn['title'];?></a>
<?php endif; ?>
<?php
$members = get_field('members');
if($members): ?>
	<div class="container">
		<?php include('inc/block-title.php');?>
	    <div class="items">
	    	<?php foreach ($members as $val): ?>
	    	<div class="item">
	    		<div class="img ir ir-3-4">
	    			<?php 
	    			if(!$val['photo']):
	    			    $val['photo'] = array('ID' => false, 'alt' => '');
	    			endif;
	    			?>
	    			<?=getImage('square_@1x', $val['photo']['ID'], array('alt' => $val['photo']['alt']));?>
	    		</div>
	    		<div class="text">
					<div class="head">
						<div class="name h3"><?=$val['name'];?></div>
						<div class="role"><?=$val['role'];?></div>
					</div>
					<div class="description"><p><?=$val['description'];?></p></div>
				</div>
	    	</div>
	    	<?php endforeach; ?>
	    </div>
    </div>
<?php endif; ?>
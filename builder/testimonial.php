<?php 
$block_title = get_field('block_title');
$block_description = get_field('block_description');
$testimonials = get_field('testimonials');
$unique_id = md5(uniqid(rand(), true));
?>
<div class="container">
	<div class="left">
		<div class="block-title">
            <h2 class="animate fade to-right"><?=$block_title;?></h2>
            <div class="description animate fade to-left">
        		<?=$block_description;?>
        	</div>
        	<?php if(count($testimonials)> 1):?>
        	<div class="slide-custom-nav">
            	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]'><?php displaySvg('slider-left.svg');?></span>
            	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]'><?php displaySvg('slider-right.svg');?></span>
            </div>
            <?php endif; ?>
    	</div>
	</div>
	<div class="right" id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"true"}, {"mouseDrag":"false"}, {"nav":"false"}, {"items":"1"}, {"autoplay":"true"}]'>
		<?php 
		if($testimonials):
    		foreach($testimonials as $testimonial): ?>
    			<div class="el">
            		<blockquote><?php displaySvg('quote-banner.svg'); ?><?=$testimonial['testimonial'];?></blockquote>
            		<div class="author">
            			<div class="name"><?=$testimonial['author'];?></div>
            			<div class="description"><?=$testimonial['author_description'];?></div>
            		</div>
        		</div>
    		<?php 
    		endforeach;
		endif;
		?>
	</div>
</div>
<div class="container"><?php include('inc/block-title.php'); ?></div>
<?php
$slides = get_field('testimonials');
if($slides): ?>
<div class="owl-carousel owl-testimonials">
	<?php foreach ($slides as $slide): ?>
	<div class="item">
		<?php 
		if(!$slide['image']):
		  $slide['image'] = array('ID' => false, 'alt' => '');
		endif;
		?>
    		<div class="image">
    			<?=getImage('square_@0.5x', $slide['image']['ID'], array('alt' => $slide['image']['alt']));?>
    		</div>
		<div class="content">
			<div class="text"><?=displaySvg('quote.svg', false).'<p>'. $slide['text'].'</p>'; ?></div>
			<?php if($slide['author']): ?>
				<span class="author"><?=$slide['author'];?></span>
			<?php endif; ?>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>
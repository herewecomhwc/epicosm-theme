<?php
$description = get_field('description');
$default_banner=get_field('default_banner', 'config-general');
$image = get_field('image');
$banner = get_field('banner');
if(!$banner) :
    $banner=$default_banner;
endif;
if(!$image) :
    $image=$banner;
endif;
?>
<div class="text">
	<?php 
	if(!is_home() && !is_category()):
	    echo getImage('rectangle_@1x', $image['ID'], array('alt' => $image['alt'], 'class' => 'image animate to-right fade')); 
	endif; ?>
    <h1 class="title animate grow fade delay-1-5s"><?php	the_field('title_1'); ?></h1>
    <?php if($description): ?><div class="description animate to-top fade delay-2s"><?php displaySvg('quote-banner.svg'); ?><?=$description; ?></div><?php endif; ?>
</div>
<?php 
if(!is_home() && !is_category()): 
    echo getImage('rectangle_large', $banner['ID'], array('alt' => $banner['alt'], 'class' => 'bg')); 
endif;?>
<?php 
$float_btn = get_field('dealer_button', 'config-content-'.pll_current_language());
if($float_btn): ?>
<a href="<?=$float_btn['url'];?>" class="btn default vertical" target="<?=$float_btn['target'];?>"><?=$float_btn['title'];?></a>
<?php endif; ?>
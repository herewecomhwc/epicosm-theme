<?php
    $preview_img = get_field('preview_image');
    if(!$preview_img):
        $preview_img = array('ID' => false, 'alt' => '');
    endif;
?>
<div class="bg-container">
    <div class="container">
    	<?php 
    	$iframe = get_field('iframe_youtube');
    	$iframe = addClass($iframe, 'skip-lazy');
    	?>
    	<?=$iframe;?>
    </div>
    <?=getImage('rectangle_large', $preview_img['ID'], array('alt' => $preview_img['alt'], 'class' => 'hover'));?>
    <div class="play-content">
    	<span class="play-icon animate fade to-top delay-1-5s"><?php displaySvg('video-play-button.svg'); ?></span>
	</div>
</div>
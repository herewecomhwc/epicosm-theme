<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="builder"><?php the_content(); ?></div>
	</div>
</article>

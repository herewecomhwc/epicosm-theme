<?php global $cid;?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="builder"><?= apply_filters( 'the_content', get_post_field( 'post_content', $cid ) ); ?></div>
	</div>
</article>

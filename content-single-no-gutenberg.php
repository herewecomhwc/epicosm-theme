<?php global $blogpage_id; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="builder">
			<div class="page-item top-banner">
			<?php
                if(has_post_thumbnail()) :
                	$banner_url=array(
                	    'alt' => '',
                	    'ID' => get_post_thumbnail_id(),
                	);
                	$banner_url_2 = get_field('second_img', get_the_ID());
                else :
                	$banner_url=get_field('default_banner','config-general');
                	$banner_url_2= false;
                endif;
                ?>
                <?php 
                    echo getImage('rectangle', $banner_url['ID'], array('alt' => $banner_url['alt'], 'class' => 'bg')); 
                    if($banner_url_2):
                        echo getImage('rectangle', $banner_url_2['ID'], array('alt' => $banner_url_2['alt'], 'class' => 'bg')); 
                    endif;
                ?>
			</div>
            <div class="page-item list no-gutenberg">
            	<div class="container">
            		<div class="list-container blocks-display-column images-display-classic column-count-1">
            			<div class="item">
            				<div class="image">
            					<div class="label animate grow fade delay-1s">
                            		<?= get_the_category_list(); ?>
                            	</div>
            				</div>
            				<div class="text">
            					<h1 class="h3 animate to-top fade delay-1-5s"><?php the_title(); ?></h1>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
			<div class="page-item text">
				<div class="container animate to-top fade delay-1-75s editorial">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="page-item share"><?php include 'builder/share.php';?></div>
			<div class="page-item news">
				<div class="block-title">
					<h2 class="animate fade to-right"><?=__('Continuez votre lecture', 'wpb');?></h2>
					<div class="description animate fade to-left">
                        <div class="btn-container animate fade to-top <?php animationDisplayDelay(1.5); ?>">
                        	<a href="<?=get_permalink($blogpage_id)?>" class="btn default"><?=__('Retour sur Oléo-mag', 'wpb');?></a>
                        </div>
                	</div>
				</div>
				<?php
				$nb_posts = 4; // Nombre d'actualités à afficher
				include 'builder/news.php';?>
			</div>
		</div>
	</div>
</article>

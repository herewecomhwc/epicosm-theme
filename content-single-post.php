<?php global $cid; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="builder">
			<?php the_content(); ?>
			<div class="page-item share"><?php include 'builder/share.php';?></div>
			<div class="page-item news">
				<?php
				$nb_posts = 2; // Nombre d'actualités à afficher
				include 'builder/news.php';?>
			</div>
		</div>
	</div>
</article>

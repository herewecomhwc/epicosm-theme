	<?php global $home_id, $cid;?>
	</div><!-- .site-content -->
	</div><!-- #content-full -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="lvl1">
			<div class="container">
				<div class="block block-messages">
					<div class="content">
						<?php 
						$messages = get_field('messages_block_groups', 'config-content-'.pll_current_language());
						if($messages):
						  foreach($messages as $message_group):
						      ?>
						      <div class="messages">
						      	<?php foreach ($message_group['messages'] as $message): ?>
						      		<div class="message type-<?=$message['type'];?>">
						      			<?=$message['message']; ?>
						      		</div>
						      	<?php endforeach;?>
						      </div>
						      <?php 
						  endforeach;
						endif;
						?>
					</div>
				</div>
    			<div class="block block-mailchimp">
    				<div class="title"><?= get_field('newsletter_block_title', 'config-content-'.pll_current_language()); ?></div>
    				<div class="content">
    					<div class="form">
							<?= do_shortcode("[hwcsendinblue]"); ?>
    					</div>
    				</div>
    			</div>
			</div>
		</div>
		<div class="lvl2">
			<div class="container">
				<div class="block block-logo">
    				<div class="content">
        				<?php $logo_site = get_field('logo_black', 'config-general'); ?>
        				<img src="<?= $logo_site['url']; ?>" alt="<?= $logo_site['alt']; ?>" />
        				<p><?=sprintf( __( '%s &copy; %d  | Tous droits réservés', 'wpb' ), get_bloginfo('name'), date('Y') );?></p>
        				<?= do_shortcode("[hwcmenu theme_location='footer']"); ?>
        				<?=get_field('copyright_footer', 'config-hwc'); ?>
    				</div>
    			</div>
    			<div class="block block-contact">
    				<div class="title"><?= get_field('contact_block_title', 'config-content-'.pll_current_language()); ?></div>
    				<div class="content">
    					<?php 
        					$localisation = do_shortcode("[hwccontact field='address']").'<br/>'.do_shortcode("[hwccontact field='postal_code']").' '.do_shortcode("[hwccontact field='city']").' '.do_shortcode("[hwccontact field='country']");
        					$phone = '<span>'.do_shortcode("[hwccontact field='phone' linked=1]").'</span>';
        					$email = do_shortcode("[hwccontact field='e-mail' linked=1]");
        					echo $localisation;
        					echo '<span class="mail">'.$email.'</span>';
        					echo $phone;
    					?>
    				</div>
    			</div>
    			<div class="block block-menu-1">
    				<div class="title"><?= get_field('menu_epicosm_block_title', 'config-content-'.pll_current_language()); ?></div>
    				<div class="content">
    					<?= do_shortcode("[hwcmenu theme_location='footer_epicosm']"); ?>
    				</div>
    			</div>
    			<div class="block block-menu-2">
    				<div class="title"><?= get_field('menu_discover_block_title', 'config-content-'.pll_current_language()); ?></div>
    				<div class="content">
    					<?= do_shortcode("[hwcmenu theme_location='footer_discover']"); ?>
    				</div>
    			</div>
    		</div>
		</div>
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

<?php
$ua_code = get_field('analytics_ua_code', 'config-hwc');
if($ua_code):
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?=$ua_code;?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?=$ua_code;?>');
</script>
<?php
endif;
?>

</body>
</html>

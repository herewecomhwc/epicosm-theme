<?php

/* ###################################################################################
 * Theme init & settings
 * ################################################################################### */

function wpb_setup() {
	load_theme_textdomain( 'wpb', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
    
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	
	remove_theme_support( 'core-block-patterns' );

	register_nav_menus( array(
		'primary' => __( 'Menu principal', 'wpb' ),
		'footer'  => __( 'Menu pied de page - Logo', 'wpb' ),
		'footer_epicosm'  => __( 'Menu pied de page - Epicosm', 'wpb' ),
		'footer_discover'  => __( 'Menu pied de page - Découvrir', 'wpb' ),
	) );

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	add_image_size( 'rectangle', 1600, 1160, true );
	add_image_size( 'rectangle_@1.5x', 1280, 928, true );
	add_image_size( 'rectangle_@1x', 800, 580, true );
	add_image_size( 'rectangle_@0.5x', 400, 290, true );
	
	add_image_size( 'portrait', 1160, 1600, true );
	add_image_size( 'portrait_@1.5x', 928, 1280, true );
	add_image_size( 'portrait_@1x', 580, 800, true );
	add_image_size( 'portrait_@0.5x', 290, 400, true );

	/*
	 * NC = No crop.
	 * Utile pour les logos par exemple.
	 */

	add_image_size( 'rectangle_nc', 1600, 1160, false );
	add_image_size( 'rectangle_nc_@1.5x', 1280, 928, false );
	add_image_size( 'rectangle_nc_@1x', 800, 580, false );
	add_image_size( 'rectangle_nc_@0.5x', 400, 290, false );

	/*
	 * Les images au format bannière (rectangle_large) ne conservent en général pas leur ratio, et finisse sur un format carré,
	 * on ne génére donc pas de taille mobile pour éviter d'avoir une image pas assez haute et donc déformée.
	 */
	add_image_size( 'rectangle_large', 1900, 640, true );
	add_image_size( 'rectangle_large_@1.5x', 1617, 544, true );
	add_image_size( 'rectangle_large_@1x', 1235, 416, true );

	add_image_size( 'square', 1600, 1600, true );
	add_image_size( 'square_@1.5x', 1280, 1280, true );
	add_image_size( 'square_@1x', 800, 800, true );
	add_image_size( 'square_@0.5x', 400, 400, true );

	add_theme_support( 'editor-styles' );
	add_editor_style( 'assets/css/editor-style.css');

}
add_action( 'after_setup_theme', 'wpb_setup' );

function clearCacheOfAdminEditorCss( $mce_init ) {

	$mce_init['cache_suffix'] = "v=" . rand(100000000,999999999);

	return $mce_init;
}
// Décommentez pour supprimer le cache du fichier editor-style.css. Ne pas laisser décommenté en production.
//add_filter( 'tiny_mce_before_init', 'clearCacheOfAdminEditorCss' );

/* ###################################################################################
 * Ajoute des formats d'image lors de l'insertion des medias dans les posts / pages
 * ################################################################################### */

add_filter('image_size_names_choose', 'setCustomImageSizesOnEditor');
function setCustomImageSizesOnEditor($sizes) {
	//$toadd = get_intermediate_image_sizes(); p($toadd);
	$addsizes = array(
	   "rectangle_@1x" => __( "Rectangle", 'wpb'),
	   "rectangle_nc_@1x" => __( "Rectangle non coupé", 'wpb'),
	   "rectangle_large_@1x" => __( "Bannière", 'wpb'),
	   "square_@1x" => __( "Carré", 'wpb'),
	);
	return $addsizes;
	/*$newsizes = array_merge($sizes, $addsizes);
	return $newsizes;*/
}

/* ###################################################################################
 * Ajoute Classe js / no-js sur body
 * ################################################################################### */

function wpb_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'wpb_javascript_detection', 0 );

/* ###################################################################################
 * Ajout des scripts / css du theme
 * ################################################################################### */

function wpb_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style( 'main.css', get_template_directory_uri().'/assets/css/main.css' );
	wp_enqueue_style( 'owl.carousel.css', get_template_directory_uri().'/assets/css/owl.carousel.min.css' );
	wp_enqueue_style( 'owl.transitions.css', get_template_directory_uri().'/assets/css/owl.transitions.css' );
	wp_enqueue_style( 'fontawesome-all.min.css', get_template_directory_uri().'/assets/css/fontawesome-all.min.css' );

	wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'plugins.js', get_template_directory_uri().'/assets/js/plugins.min.js', array('jquery', 'magnific-popup', 'owl.carousel.min.js', 'parallax', 'count-to', 'jquery.appear'), '1.0.0', true );
	wp_enqueue_script( 'theme.js', get_template_directory_uri().'/assets/js/theme.min.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'ajaxpaginate.js', get_template_directory_uri().'/assets/js/ajaxpaginate.min.js', array('jquery'), '1.0.0', true );

	$ajax_arr = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
	);
	wp_localize_script( 'ajaxpaginate.js', 'ajaxpagination', $ajax_arr);


	wp_enqueue_script( 'owl.carousel.min.js', get_template_directory_uri().'/assets/js/owl.carousel.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'parallax', get_template_directory_uri().'/assets/js/parallax.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'jquery.appear', get_template_directory_uri().'/assets/js/jquery.appear.js', array(), '1.0.0', true );
	wp_enqueue_script( 'count-to', get_template_directory_uri().'/assets/js/count-to.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri().'/assets/js/magnific-popup.min.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpb_scripts' );

/* ###################################################################################
 * Autres fonctions
 * ################################################################################### */
if ( !is_admin()):

	if( !function_exists('pll_languages_list') ) { function pll_languages_list(){return array();}}
	if( !function_exists('pll_current_language') ) { function pll_current_language(){}}
	if( !function_exists('get_field_object') ) { function get_field_object(){}}
	if( !function_exists('get_field') ) {	function get_field() {}}
	if( !function_exists('the_field') ) { function the_field(){}}
	if( !function_exists('pll_get_post') ) { function pll_get_post(){}}
	if( !function_exists('pll_current_language') ) { function pll_current_language(){}}

endif;

include 'functions/init.php';
include 'functions/shortcodes.php';
include 'functions/tools.php';
include 'functions/cpt.php';
include 'functions/display.php';
include 'functions/plugins.php';
if(is_woocommerce_activated() && file_exists(dirname(__FILE__).'/functions/woocommerce.php')):
    include 'functions/woocommerce.php';
endif;
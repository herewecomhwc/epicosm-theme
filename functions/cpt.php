<?php

/* ###################################################################################
 * Init des CPT créateurs de CPT / Taxonomies
 * ################################################################################### */

add_action( 'init', 'initBaseCpt' );
function initBaseCpt()
{
	// CUSTOM POST TYPES
	$cpts = array(
			array(
					'plural_name' => 'Custom post types',
					'singular_name' => 'Custom post type',
					'cpt_id' => 'hwc-cpt',
					'gender' => 'male',
			),
			array(
					'plural_name' => 'Taxonomies',
					'singular_name' => 'Taxonomie',
					'cpt_id' => 'hwc-taxo',
					'gender' => 'female',
			),
			array(
					'plural_name' => 'Gutenberg blocks',
					'singular_name' => 'Gutenberg block',
					'cpt_id' => 'hwc-gutenberg',
					'gender' => 'male',
			),
	);
	if($cpts && !empty($cpts)):
	$i = 10;
	foreach ($cpts as $cpt):
	$plural_name = $cpt['plural_name'];
	$singular_name = $cpt['singular_name'];
	$cpt_id = $cpt['cpt_id'];
	$gender = $cpt['gender'];
	if($gender == 'male'):
	$text_all = 'Tous';
	$text_new = 'Nouveau';
	$text_no = 'Aucun';
	elseif($gender == 'female'):
	$text_all = 'Toutes';
	$text_new = 'Nouvelle';
	$text_no = 'Aucune';
	endif;
	/* Labels */
	$labels_cpt = array(
			'name'               => _x( $plural_name, 'post type general name', 'wpb' ),
			'singular_name'      => _x( $singular_name, 'post type singular name', 'wpb' ),
			'menu_name'          => _x( $plural_name, 'admin menu', 'wpb' ),
			'name_admin_bar'     => _x( $singular_name, 'add new on admin bar', 'wpb' ),
			'add_new'            => _x( 'Ajouter '.$text_new, $cpt_id, 'wpb' ),
			'add_new_item'       => __( 'Ajouter '.$text_new.' '.$singular_name, 'wpb' ),
			'new_item'           => __( $text_new.' '.$singular_name, 'wpb' ),
			'edit_item'          => __( 'Editer '.$singular_name, 'wpb' ),
			'view_item'          => __( 'Voir '.$singular_name, 'wpb' ),
			'all_items'          => __( $plural_name, 'wpb' ),
			'search_items'       => __( 'Rechercher '.$plural_name, 'wpb' ),
			'parent_item_colon'  => __( $singular_name.' parent:', 'wpb' ),
			'not_found'          => __( $text_no.' '.$singular_name.' trouvé.', 'wpb' ),
			'not_found_in_trash' => __( $text_no.' '.$singular_name.' trouvé dans la corbeille.', 'wpb' )
	);

	/* Args */
	$args_cpt = array(
			'labels'             => $labels_cpt,
			'show_ui'            => true,
			'show_in_admin_bar'            => false,
			'show_in_menu'       => 'config-build',
			'menu_position'       => $i,
			'supports'			=> array('title'),
	);
	register_post_type( $cpt_id, $args_cpt);
	$i++;
	endforeach;
	endif;
}

/* ###################################################################################
 * Init des customs post type, taxos et blocs générés via les CPT
 * ################################################################################### */

add_action( 'init', 'initAllCpt' );
function initAllCpt()
{
	// CUSTOM POST TYPES
	$cpts = new WP_Query(array(
			'post_type' => 'hwc-cpt',
			'posts_per_page' => -1,
	        'post_status' => array('publish'),
	));
	if($cpts->have_posts()):
		while ($cpts->have_posts()):
			$cpts->the_post();
			$plural_name = get_field('plural_name');
			$singular_name = get_field('singular_name');
			$cpt_id = get_field('cpt_id');
			$gender = get_field('gender');
			$supports = get_field('supports');
			if(empty($supports)): $supports = false; endif;
			if($gender == 'male'):
				$text_all = 'Tous';
				$text_new = 'Nouveau';
				$text_no = 'Aucun';
			elseif($gender == 'female'):
				$text_all = 'Toutes';
				$text_new = 'Nouvelle';
				$text_no = 'Aucune';
			endif;
			/* Labels */
			$labels_cpt = array(
				'name'               => _x( $plural_name, 'post type general name', 'wpb' ),
				'singular_name'      => _x( $singular_name, 'post type singular name', 'wpb' ),
				'menu_name'          => _x( $plural_name, 'admin menu', 'wpb' ),
				'name_admin_bar'     => _x( $singular_name, 'add new on admin bar', 'wpb' ),
				'add_new'            => _x( 'Ajouter '.$text_new, $cpt_id, 'wpb' ),
				'add_new_item'       => __( 'Ajouter '.$text_new.' '.$singular_name, 'wpb' ),
				'new_item'           => __( $text_new.' '.$singular_name, 'wpb' ),
				'edit_item'          => __( 'Editer '.$singular_name, 'wpb' ),
				'view_item'          => __( 'Voir '.$singular_name, 'wpb' ),
				'all_items'          => __( $text_all.' les '.$plural_name, 'wpb' ),
				'search_items'       => __( 'Rechercher '.$plural_name, 'wpb' ),
				'parent_item_colon'  => __( $singular_name.' parent:', 'wpb' ),
				'not_found'          => __( $text_no.' '.$singular_name.' trouvé.', 'wpb' ),
				'not_found_in_trash' => __( $text_no.' '.$singular_name.' trouvé dans la corbeille.', 'wpb' )
			);

			/* Args */
			$args_cpt = array(
				'labels'             => $labels_cpt,
		        'public'             => true,
				'show_ui'            => true,
		        'show_in_menu'       => true,
				'rewrite'            => array( 'slug' => get_field('rewrite')),
				'capability_type'    => 'post',
				'has_archive'        => get_field('has_archive'),
				'hierarchical'       => get_field('hierarchical'),
				'menu_position'      => 21,
				'menu_icon'			 => get_field('menu_icon'),
				'supports'           => $supports,
				'exclude_from_search' => get_field('exclude_from_search'),
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => get_field('show_in_nav_menus'),
				'publicly_queryable'  => get_field('publicly_queryable'),
				'query_var'           => false,
				'show_in_rest'	 	  => true,
			);
			register_post_type( $cpt_id, $args_cpt);
		endwhile;
		wp_reset_postdata();
	endif;

	// TAXONOMIES

	$taxos = new WP_Query(array(
		'post_type' => 'hwc-taxo',
		'posts_per_page' => -1,
		'post_status' => array('publish'),
	));
	if($taxos->have_posts()):
		while ($taxos->have_posts()):
			$taxos->the_post();
			$plural_name = get_field('plural_name');
			$singular_name = get_field('singular_name');
			$taxo_id = get_field('taxo_id');
			$gender = get_field('gender');
			if($gender == 'male'):
				$text_all = 'Tous';
				$text_new = 'Nouveau';
				$text_no = 'Aucun';
			elseif($gender == 'female'):
				$text_all = 'Toutes';
				$text_new = 'Nouvelle';
				$text_no = 'Aucune';
			endif;
			/* Labels */
			$labels_taxo = array(
					'name'               => _x( $plural_name, 'post type general name', 'wpb' ),
					'singular_name'      => _x( $singular_name, 'post type singular name', 'wpb' ),
					'menu_name'          => _x( $plural_name, 'admin menu', 'wpb' ),
					'add_new_item'       => __( 'Ajouter '.$text_new.' '.$singular_name, 'wpb' ),
					'edit_item'          => __( 'Editer '.$singular_name, 'wpb' ),
					'view_item'          => __( 'Voir '.$singular_name, 'wpb' ),
					'all_items'          => __( $text_all.' les '.$plural_name, 'wpb' ),
					'search_items'       => __( 'Rechercher '.$plural_name, 'wpb' ),
					'parent_item_colon'  => __( $singular_name.' parent:', 'wpb' ),
					'not_found'          => __( $text_no.' '.$singular_name.' trouvé.', 'wpb' ),
			);
			/* Args */
			$args_taxo = array(
					'labels'             => $labels_taxo,
					'public'             => true,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'rewrite'            => array( 'slug' => get_field('rewrite')),
					'hierarchical'       => get_field('hierarchical'),
					'show_in_nav_menus'   => get_field('show_in_nav_menus'),
					'publicly_queryable'  => get_field('publicly_queryable'),
					'query_var'           => false,
					'show_in_rest'           => true,
			);
			$custom_post_type = get_field('custom_post_type');
			if(strpos($custom_post_type, ',') !== false){
				$custom_post_type = explode(',', str_replace(' ', '', $custom_post_type));
			}
			register_taxonomy( $taxo_id, $custom_post_type, $args_taxo);

			add_rewrite_rule(
			    '^'.get_field('rewrite').'/([^/]*)/([^/]*)/(\d*)?',
			    'index.php?'.$taxo_id.'=$matches[1]&p=$matches[2]&paged=$matches[3]',
			    'top'
			    );
		endwhile;
		wp_reset_postdata();
	endif;
}


/* ###################################################################################
 * On recupere la page servant de listing au CPT (défini dans
 * le champs ACF "cpt_homepage"
 * ################################################################################### */

function get_cpt_homepage_id($cpt) {
	$cpt = new WP_Query(array(
	'post_type' => 'hwc-cpt',
	'meta_query' => array(
		array(
			'key' => 'cpt_id',
			'value' => $cpt,
		),
	)));
	if($cpt->have_posts()):
		while($cpt->have_posts()):
			$cpt->the_post();
			$cid = get_field('cpt_homepage', get_the_ID());
		endwhile;
			wp_reset_postdata();
	else:
		$cid = false;
	endif;
	return pll_get_post($cid);
}
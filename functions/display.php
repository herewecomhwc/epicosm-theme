<?php


/* ###################################################################################
 * Afichage des galleries
 * ################################################################################### */

add_filter('post_gallery', 'displayPostGalleryOutput', 10, 2);
function displayPostGalleryOutput($output, $attr) {
	global $post;

	if (isset($attr['orderby'])) {
		$attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
		if (!$attr['orderby'])
			unset($attr['orderby']);
	}

	extract(shortcode_atts(array(
			'order' => 'ASC',
			'orderby' => 'menu_order ID',
			'id' => $post->ID,
			'itemtag' => 'dl',
			'icontag' => 'dt',
			'captiontag' => 'dd',
			'columns' => 3,
			'size' => 'thumbnail',
			'include' => '',
			'exclude' => ''
	), $attr));

	$id = intval($id);
	if ('RAND' == $order) $orderby = 'none';

	if (!empty($include)) {
		$include = preg_replace('/[^0-9,]+/', '', $include);
		$_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby, 'posts_per_page' => 5,));

		$attachments = array();
		foreach ($_attachments as $key => $val) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	}

	if (empty($attachments)) return '';

	// Here's your actual output, you may customize it to your need
	$output = "<div class=\"wp-gallery\">\n";
	// Now you loop through each attachment
	$gal_id = md5(uniqid(rand(), true));
	foreach ($attachments as $id => $attachment) {

		$img_full = wp_get_attachment_image_src($id, 'rectangle_nc');
		$output .= "<a href='".$img_full[0]."' class='popin ir ir-hover ir-rectangle' rel='gal-".$gal_id."'>";
		$output .= wp_get_attachment_image($id, 'rectangle_@1x');
		$output .= "</a>";
	}

	$output .= "</ul>\n";
	$output .= "</div>\n";

	return $output;
}

/* ###################################################################################
 * Menu custom
 * ################################################################################### */

class HWC_Walker extends Walker_Nav_Menu{

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        // RECUPERATION DES DIFFERENTS ID
        $post_id = $item->object_id;
        $cur_template = get_page_template_slug( $post_id );
        $post_parent_id = wp_get_post_parent_id( $post_id );
        $post_parent_template = get_page_template_slug( $post_parent_id );
        $submenu_type = get_field('submenu_type', $item);
        if(!$submenu_type):
            $submenu_type = 'classic';
        endif;
        // - RECUPERATION DES DIFFERENTS ID

        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        $classes[] = $submenu_type;


        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );


        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';


        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters( 'the_title', $item->title, $item->ID );


        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

        $item_output = '';
        if($submenu_type == 'classic' || $submenu_type == 'classic_parent'):
            $item_output .= $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . $title . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
        elseif($submenu_type == 'list'):
            $list = get_field('list', $item);
            $item_output .= $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . $title . $args->link_after;
            $item_output .= '</a>';
            $item_output .= '<div class="list">';
            foreach($list as $litem):
                $item_output .= '<div class="litem"><a href="'.get_permalink($litem['post']).'">'.get_the_title($litem['post']).'</a></div>';
            endforeach;
            $item_output .= '</div>';
            $item_output .= $args->after;
        elseif($submenu_type == 'block'):
            $block_image = get_field('block_image', $item);
            $block_title = get_field('block_title', $item);
            $block_description = get_field('block_description', $item);
            $block_lien = get_field('block_lien', $item);
            $item_output .= '<div class="image">'.getImage('rectangle_@1x', $block_image['ID']).'</div>';
            $item_output .= '<div class="title">'.$block_title.'</div>';
            $item_output .= '<div class="description">'.$block_description.'</div>';
            $item_output .= '<a href="'.get_permalink($block_lien).'"></a>';
        endif;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }


    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= "</li>{$n}";
    }


}

/* ###################################################################################
 * Generation des attributs srcset
 * ################################################################################### */

function displaySrcset($image_id,$image_size,$echo = true, $max_width = '1900px'){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
		$return = 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
		// generate the markup for the responsive image
		if($echo === true):
		  echo $return;
		else:
		  return $return;
		endif;
	}
}

/* ###################################################################################
 * Pagination AJAX
 * ################################################################################### */

add_action( 'wp_ajax_nopriv_ajax_pagination', 'displayAjaxPagination' );
add_action( 'wp_ajax_ajax_pagination', 'displayAjaxPagination' );

function displayAjaxPagination() {
	$args = $_POST['args'];
	$args['paged'] = $_POST['page'];
	$template = $_POST['template'];
	$posts = new WP_Query($args);
	if( $posts->have_posts() ):
	    $delay = 1;
		while ( $posts->have_posts() ):
			$posts->the_post();
			get_template_part( 'item-list', $template, array('delay' => $delay) );
			animationIncreaseDelay($delay);
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}

/* ###################################################################################
 * Affiche tous les réseaux sociales du site
 * ################################################################################### */

 function displaySocialNetworks() {
     echo do_shortcode('[hwcsocial network="facebook" fa="fa-facebook-square"]');
     echo do_shortcode('[hwcsocial network="twitter" fa="fa-twitter"]');
     echo do_shortcode('[hwcsocial network="linkedin" fa="fa-linkedin-in"]');
     echo do_shortcode('[hwcsocial network="viadeo" fa="fa-viadeo"]');
     echo do_shortcode('[hwcsocial network="youtube" fa="fa-youtube"]');
     echo do_shortcode('[hwcsocial network="vimeo" fa="fa-vimeo-v"]');
     echo do_shortcode('[hwcsocial network="instagram" fa="fa-instagram"]');
     echo do_shortcode('[hwcsocial network="pinterest" fa="fa-pinterest"]');
     echo do_shortcode('[hwcsocial network="tripadvisor" fa="fa-tripadvisor"]');
 }

 /* ###################################################################################
  * Pagination des articles
  * Requiert :
  * - #ajax-target sur le parent
  * - Les items enfants (html) doivent etre directement sous le parent depuis un fichier
  *  dédié nommé 'item-list-xxxx.php'. xxx correspondant à la valeur du parametre 'template'
  * - le template en parametre (3eme argument)
  * - l'objet wp_query (1er argument) sur les requetes non archives
  * ################################################################################### */

 function displayPagination ($query = '', $args = array(), $template = 'news')
 {
     global $wp_query;
     if( get_query_var('paged') ) {
         $paged = get_query_var('paged');
     }else if ( get_query_var('page') ) {
         $paged = get_query_var('page');
     }else{
         $paged = 1;
     }

     if($query):
        $maxpages = $query->max_num_pages;
     else:
        $maxpages = $wp_query->max_num_pages;
     endif;

     $html_params = array(
         'maxpages' => $maxpages,
         'paged' => $paged,
         'template' => $template,
     );

     include dirname(__FILE__) .'/../parts/paginate.php';

     $query_args=array (
         'lang' => pll_current_language(),
         'paged' => $paged+1,
         'posts_per_page' => 10
     );

     if(is_category()) :
        $query_args['cat']=get_queried_object()->term_id;
     endif;

     if(!empty($args) && is_array($args)):
         foreach ($args as $key => $value):
            $query_args[$key] = $value;
         endforeach;
     endif;
     wp_localize_script('ajaxpaginate.js','ajaxargs',$query_args);

     $arr = array('before' => "<div class='paginate'>", 'after' => '</div>');
     if($query):
        $arr['query'] = $query;
     endif;
     wp_pagenavi($arr);

 }

 /* ###################################################################################
  * Affiche la chaine en remplacant . et @ par d'autres chaines gerées elle en js
  * ################################################################################### */

 function displayEmail($str, $echo = true)
 {
     if(is_admin()):
        return $str; // Rendu Gutenberg
     endif;
     $str = str_replace("@", "@no-spam-", $str);
     if($echo):
        echo $str;
     else:
        return $str;
     endif;
 }

 /* ###################################################################################
  * Inclusion de SVG en gérant le .htaccess herewecom
  * ################################################################################### */

 function displaySvg($filename, $echo = true, $direct_link = false) {
     if(strpos($filename, get_site_url()) !== false): // Si on fait un appel vers notre propre site, on traduit ça en url relative
         $filename = str_replace(get_site_url().'/', '', $filename);
         $return = file_get_contents($filename);
     else:
         if($_SERVER['SERVER_NAME'] == 'dev.herewecom.fr'):
             $context = stream_context_create(array (
                 'http' => array (
                     'header' => 'Authorization: Basic ' . base64_encode("hwc:hwc")
                 )
             ));
             if($direct_link):
                $return = file_get_contents($filename, false, $context);
             else:
                $return = file_get_contents(get_stylesheet_directory_uri()."/assets/images/svg/".$filename, false, $context);
             endif;
         else:
             if($direct_link):
                $return = file_get_contents($filename);
             else:
                $return = file_get_contents(get_stylesheet_directory()."/assets/images/svg/".$filename);
             endif;
         endif;
     endif;
     if($echo):
        echo $return;
     else:
        return $return;
     endif;
 }
 
 /* ###################################################################################
  * Functions permetttant de gérer le délai d'affichage des animations
  * ################################################################################### */
 
 function animationIncreaseDelay(&$delay, $increase = 0.25) {
     $delay = $delay + $increase;
     if($delay > 5): $delay = 0.5; endif;
     return $delay;
 }
 
 function animationDisplayDelay($delay, $echo = true) {
     $print = 'delay-'.str_replace('.', '-', $delay).'s';
     if($echo):
        echo $print;
     else:
        return $print;
     endif;
 }
 
 /* ###################################################################################
  * Récupere les informations d'une image ou les affiche. Permet également un affichage 
  * type "demo" avec des images aléatoire de Unsplash.
  * ################################################################################### */
 
 function isDemoModeOn() {
     return get_field('demo_mode', 'config-hwc');
 }
 
 function getImage($size, $attachment_id = false, $attr = array(), $return = 'html') {
    if(!$attachment_id): // Si $id non défini on admet que c'est pour le post courant
        $post_id = get_the_ID();
        $attachment_id = get_post_thumbnail_id($post_id);
    endif;
    $result = array();
    if($return == 'data' || $return == 'full'):
        $image = wp_get_attachment_image_src($attachment_id, $size, false);
        if(isDemoModeOn() && !$image):
            global $_wp_additional_image_sizes;
            $image = array("https://source.unsplash.com/random?sig=".rand(), $_wp_additional_image_sizes[$size]['width'], $_wp_additional_image_sizes[$size]['height'], true);
        endif;
        $result['data'] = $image;
     endif;
     if($return == 'url' || $return == 'full'):
         $data = wp_get_attachment_image_src($attachment_id, $size, false);
         if($data):
            $image = $data[0];
         else:
            $image = false;
         endif;
         if(isDemoModeOn() && !$image):
            $image = "https://source.unsplash.com/random?sig=".rand();
         endif;
         $result['url'] = $image;
     endif;
     if($return == 'html' || $return == 'full'):
         $image = wp_get_attachment_image($attachment_id, $size, false, $attr);
         if(isDemoModeOn() && empty($image)):
             global $_wp_additional_image_sizes;
             $image = '<img src="https://source.unsplash.com/random?sig='.rand().'"';
             if(!empty($attr)):
                foreach($attr as $attribute => $value):
                    $image .= ' '.$attribute.'="'.$value.'"';
                endforeach;
             endif;
             $image .= ' />';
         endif;
         $result['html'] = $image;
     endif;
     if($return == 'full'):
         $result['full'] = array(
             'data' => $result['data'],
             'url' => $result['url'],
             'html' => $result['html'],
         );
     endif;
    return $result[$return];
 }
 
 /* ###################################################################################
  * Affichage une carte avec des marqueurs fournis
  * ################################################################################### */
 
 function displayHwcMap($markers = array(), $atts = array(), $display = true) {
     ob_start();
     $div_id = $atts['div_id'];
     
     if(isset($atts['marker_img'])):
     $marker_img = $atts['marker_img'];
     else:
     $marker_img = 'marker-gmap.png';
     endif;
     
     if($markers): ?>
		<div id="<?= $div_id;?>" class="map"></div>
		<script type="text/javascript">
			function initMap() {
				var mapOptions = {
					disableDefaultUI: true,
					styles: <?php the_field('snazzy_maps', 'config-hwc'); ?>,
					//mapTypeControl: true,
					//zoomControl: true,
					//streetViewControl: true,
		            /*streetViewControlOptions: {
		                position: google.maps.ControlPosition.LEFT_TOP
		            },
		            zoomControlOptions: {
		                position: google.maps.ControlPosition.LEFT_TOP
		            },*/
				};
				var mapElement = document.getElementById("<?= $div_id; ?>");

				var map = new google.maps.Map(mapElement, mapOptions);
				var markerBounds = new google.maps.LatLngBounds();
				<?php 
				$marker_center_position = get_field('marker_center_position', 'config-hwc');
				if($marker_center_position == 'center'):
				?>
    				var image = {
    					url: '<?= get_stylesheet_directory_uri(); ?>/assets/images/<?= $marker_img; ?>',
    					size : new google.maps.Size(100, 100),
    					scaledSize : new google.maps.Size(100, 100),
    					origin: new google.maps.Point(0, 0),
    					anchor: new google.maps.Point(50, 50)
    				};	
				<?php else: ?>
					var image = '<?= get_stylesheet_directory_uri(); ?>/assets/images/<?= $marker_img; ?>';
				<?php endif; ?>
				var activeInfoWindow;
				<?php
				$marker_i = 0;
				foreach ($markers as $marker): ?>
					var point = new google.maps.LatLng(<?= $marker['localisation']['lat']; ?>, <?= $marker['localisation']['lng']; ?>);
					var marker<?= $marker_i;?> = new google.maps.Marker({
						position: point,
						map: map,
						<?php if($marker_img): ?>icon: image<?php endif;?>
					});
					markerBounds.extend(point );
					<?php
					   if($marker['localisation_description']):
    						$contentString = str_replace("\"", "'", $marker['localisation_description']);
    						$contentString = str_replace("\n","",$contentString);
    						$contentString = str_replace("\r","",$contentString);
    						$contentString = str_replace("\t","",$contentString);

					?>
					var contentString<?= $marker_i;?> = "<?= $contentString; ?>";
					var infowindow<?= $marker_i;?> = new google.maps.InfoWindow({
						content: contentString<?= $marker_i;?>
					});
					<?php
					   endif;
					?>
					marker<?= $marker_i;?>.addListener("click", function() {
						if (activeInfoWindow) { activeInfoWindow.close();}
						infowindow<?= $marker_i;?>.open(map, marker<?= $marker_i;?>);
						activeInfoWindow = infowindow<?= $marker_i;?>;
					});
					<?php
					$marker_i++;
				endforeach;?>
				var zoom_level_value = 0.1;
				if (markerBounds.getNorthEast().equals(markerBounds.getSouthWest())) {
				       var extendPoint1 = new google.maps.LatLng(markerBounds.getNorthEast().lat() + zoom_level_value, markerBounds.getNorthEast().lng() + zoom_level_value);
				       var extendPoint2 = new google.maps.LatLng(markerBounds.getNorthEast().lat() - zoom_level_value, markerBounds.getNorthEast().lng() - zoom_level_value);
				       markerBounds.extend(extendPoint1);
				       markerBounds.extend(extendPoint2);
				    }

				map.fitBounds(markerBounds);
				<?php if(isset($atts['geojson']) && !empty($atts['geojson'])): ?>
    				map.data.loadGeoJson(
                        "<?=get_stylesheet_directory_uri(); ?>/parts/geojson/<?=$atts['geojson'];?>.geojson"
                      );     
                      map.data.setStyle({    
                           fillColor: '#ff8364',
                           strokeColor: '#ff8364',
                           fillOpacity: 0.15,
                           strokeOpacity: 0.5,
                           strokeWeight: 2,
                        });          
                <?php endif; ?>
                

			}
			defer(function () {
				jQuery(document).ready(function($){
					initMap();
				});
			});
		</script>
	<?php
	else:
		echo "<p class='alert error'>[Erreur de configuration, merci de vérifier les paramètres du shortcode de la carte]</p>";
	endif;
	$result = ob_get_clean();
	if($display):
	   echo $result;
	else:
	   return $result;
	endif;
 }

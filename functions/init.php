<?php

/* ###################################################################################
 * Protection des pages
 * ################################################################################### */

/* @TODO current_language pas encore défini, du coup pll_get_post() return false */

function setRestrictionForPostDeletion($post_ID){
	$restricted_pages = array(
			pll_get_post(get_field('search', 'config-hwc')),
			pll_get_post(get_field('protected', 'config-hwc')),
			pll_get_post(get_field('404', 'config-hwc')),
			pll_get_post(get_option('page_for_posts')),
			pll_get_post(get_option('page_on_front'))
	);
	if(in_array($post_ID, $restricted_pages) && $post_ID !== false){
		exit('Vous ne pouvez pas supprimer cette page.');
	}
}
add_action('wp_trash_post', 'setRestrictionForPostDeletion', 10, 1);
add_action('before_delete_post', 'setRestrictionForPostDeletion', 10, 1);

add_filter( 'page_row_actions', 'setHiddenRemoveRowActions', 10, 2 );
function setHiddenRemoveRowActions( $actions, $post )
{
	$restricted_pages = array(
			pll_get_post(get_field('search', 'config-hwc')),
			pll_get_post(get_field('protected', 'config-hwc')),
			pll_get_post(get_field('404', 'config-hwc')),
			pll_get_post(get_option('page_for_posts')),
			pll_get_post(get_option('page_on_front'))
	);
	if(in_array($post->ID, $restricted_pages)){

		unset( $actions['trash'] );
		unset( $actions['delete_permanently'] );
	}
	return $actions;
}


/* ###################################################################################
 * Supprime le mauvais highlight du menu sur la "page for post" sur les 404 et search
 * ################################################################################### */

function fixWpNavMenuStrangeHighlight($sorted_menu_items, $args){
	// Vérifier que la page est bien un article du blog
	global $wp_query;
	if(!empty($wp_query->queried_object_id)){
		$current_page = get_post($wp_query->queried_object_id);
		if($current_page && $current_page->post_type=='post'){
			//yes!
		} else{
			$current_page = false;
		}
	} else {
		$current_page = false;
	}

	$home_page_id = (int) get_option( 'page_for_posts' );
	foreach($sorted_menu_items as $id => $menu_item){
		if ( ! empty( $home_page_id ) && 'post_type' == $menu_item->type && empty( $wp_query->is_page ) && $home_page_id == $menu_item->object_id ){
			if(!$current_page){
				foreach($sorted_menu_items[$id]->classes as $classid=>$classname){
					if($classname=='current_page_parent'){
						unset($sorted_menu_items[$id]->classes[$classid]);
					}
				}
			}
		}
	}
	return $sorted_menu_items;
}
add_filter('wp_nav_menu_objects','fixWpNavMenuStrangeHighlight',10,2);

/* ###################################################################################
 * Renomme les noms des fichiers upload
 * ################################################################################### */

function setSanitizedFileNameOnUpload($filename) {

	$sanitized_filename = remove_accents($filename); // Convert to ASCII

	// Standard replacements
	$invalid = array(
			' ' => '-',
			'%20' => '-',
			'_' => '-'
	);
	$sanitized_filename = str_replace(array_keys($invalid), array_values($invalid), $sanitized_filename);

	$sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
	$sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
	$sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
	$sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
	$sanitized_filename = strtolower($sanitized_filename); // Lowercase

	return $sanitized_filename;
}

add_filter('sanitize_file_name', 'setSanitizedFileNameOnUpload', 10);

/* ###################################################################################
 * Enleve les tag des posts
 * ################################################################################### */

function setUnregisteredTags() {
	unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'setUnregisteredTags');

/* ###################################################################################
 * Ajout de la variable ajaxurl utilisee pour les appel ajax
 * ################################################################################### */

add_action('wp_head','setAjaxurlVarOnFront');
function setAjaxurlVarOnFront() {
	?>
	<script type="text/javascript">
		var ajaxurl = '<?= admin_url('admin-ajax.php'); ?>';
	</script>
	<?php
}

add_filter( 'nav_menu_link_attributes', 'setClassToFakeLinks', 10, 3 );

/* ###################################################################################
 * Ajoute une classe nolink aux liens "ancres" des menus
 * ################################################################################### */

function setClassToFakeLinks( $atts, $item, $args ) {
	// Manipulate attributes
	if($atts['href'] == '#'):
		if(isset($atts['class'])):
			$atts['class'] .= ' nolink';
		else:
			$atts['class'] = 'nolink';
		endif;

	endif;
	return $atts;
}

/* ###################################################################################
 * Set locale traduction des dates
 * ################################################################################### */

add_action('init', 'setLocaleHwc');

function setLocaleHwc()
{
	$cur_locale = pll_current_language('locale');
	setlocale(LC_TIME, "");
	setlocale (LC_TIME, $cur_locale.'.utf8');
}

/* ###################################################################################
 * Api GMAP ACF
 * ################################################################################### */

function setGmapApiKeyToAcf() {
	acf_update_setting('google_api_key', get_field('gmap_api_key', 'config-hwc'));
}
add_action('acf/init', 'setGmapApiKeyToAcf');

/* ###################################################################################
 * Ajout des champs ACF dans la recherche
 * ################################################################################### */


function setAcfIncludedInSearchJoin( $join ) {
	global $wpdb;

	if ( is_search() ) {
		$join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}

	return $join;
}
add_filter('posts_join', 'setAcfIncludedInSearchJoin' );

function setAcfIncludedInSearchWhere( $where ) {
	global $pagenow, $wpdb;

	if ( is_search() ) {
		$where = preg_replace(
				"/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
				"(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	}

	return $where;
}
add_filter( 'posts_where', 'setAcfIncludedInSearchWhere' );

function setAcfIncludedInSearchDistinct( $where ) {
	global $wpdb;

	if ( is_search() ) {
		return "DISTINCT";
	}

	return $where;
}
add_filter( 'posts_distinct', 'setAcfIncludedInSearchDistinct' );

/* ###################################################################################
 * On réajoute l'éditor sur la page blog
 * ################################################################################### */

function setBackEditorOnPostsPage($post) {

	if( $post->ID != get_option( 'page_for_posts' ) ) { return; }

	remove_action( 'edit_form_after_title', '_wp_posts_page_notice' );
	add_post_type_support( 'page', 'editor' );

}

add_action( 'edit_form_after_title', 'setBackEditorOnPostsPage', 0 );

/* ###################################################################################
 * Autorise les editeurs et administrateurs à éditer la page de politique de confidentialité
 * ################################################################################### */

add_action('map_meta_cap', 'setPrivacyPolicyPageEditableByEditorAndAdministrator', 1, 4);
function setPrivacyPolicyPageEditableByEditorAndAdministrator($caps, $cap, $user_id, $args)
{
	if ('manage_privacy_options' === $cap) {
		$manage_name = is_multisite() ? 'manage_network' : 'manage_options';
		$caps = array_diff($caps, [ $manage_name ]);
	}
	return $caps;
}

/* ###################################################################################
 * Autorise l'envoi de fichier .svg dans le bibliothéque de médias
 * ################################################################################### */

function setAllowUploadsOfSVG($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'setAllowUploadsOfSVG');

/* ###################################################################################
 * Normalisation du nombre d'élements par page sur les requêtes du front
 * ################################################################################### */

add_action( 'pre_get_posts', 'setPppForPostsQueries' );

function setPppForPostsQueries ( $query ) {

    if ( $query->is_main_query() && ! $query->is_feed() && ! is_admin() && (is_category() || is_home() || is_archive() || is_search()) ) {
        $query->set( 'posts_per_page', get_field('ppp','config-posts') );
    }
}

/* ###################################################################################
 * Affiche un message d'erreur si le navigateur n'est pas à jour
 * ################################################################################### */

function setBrowserUpdateCheckerScript() {
    echo '<script>
	var $buoop = {required:{e:-4,f:-3,o:-3,s:-1,c:-3},insecure:true,api:2018.10 };
	function $buo_f(){
	 var e = document.createElement("script");
	 e.src = "//browser-update.org/update.min.js";
	 document.body.appendChild(e);
	};
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	catch(e){window.attachEvent("onload", $buo_f)}
	</script>';
}
add_action('admin_footer', 'setBrowserUpdateCheckerScript');

/* ###################################################################################
 * On change le slug de la recherche pour autoriser
 les pages à avoir le permalien /recherche/***
 * ################################################################################### */

function setCustomSearchSlug() {
    
    $GLOBALS['wp_rewrite']->search_base = 'hwc_search';
}
add_action( 'init', 'setCustomSearchSlug' );

/* ###################################################################################
 * On désactive les playlists audio et vidéo de l'ajout depuis les médias
 * ################################################################################### */

function remove_media_tab( $strings ) {

    $strings["setFeaturedImageTitle"] = "";
    $strings["insertFromUrlTitle"] = "";
    $strings['createPlaylistTitle'] = "";
    $strings['createVideoPlaylistTitle'] = "";
    return $strings;
}
add_filter( 'media_view_strings', 'remove_media_tab' );

add_filter( 'media_library_show_audio_playlist', function(){
    return false;
}, 10, 1 );
    
    add_filter( 'media_library_show_video_playlist', function(){
        return false;
    }, 10, 1 );

/* ###################################################################################
 * On défnit quels rôles peuvent modifier / supprimer quels autres rôles
 * ################################################################################### */

function wpse_188863_get_allowed_roles( $user ) {
    $allowed = array();
    
    if ( in_array( 'administrator', $user->roles ) ) { // Admin can edit all roles
        $allowed = array_keys( $GLOBALS['wp_roles']->roles );
    } elseif ( in_array( 'editor', $user->roles ) ) {
        $allowed[] = 'contributor';
        $allowed[] = 'subscriber';
    } elseif ( in_array( 'contributor', $user->roles ) ) {
        $allowed[] = 'subscriber';
    }
    
    return $allowed;
}

/**
 * Remove roles that are not allowed for the current user role.
 */
function wpse_188863_editable_roles( $roles ) {
    if ( $user = wp_get_current_user() ) {
        $allowed = wpse_188863_get_allowed_roles( $user );
        
        foreach ( $roles as $role => $caps ) {
            if ( ! in_array( $role, $allowed ) )
                unset( $roles[ $role ] );
        }
    }
    
    return $roles;
}

add_filter( 'editable_roles', 'wpse_188863_editable_roles' );

/**
 * Prevent users deleting/editing users with a role outside their allowance.
 */
function wpse_188863_map_meta_cap( $caps, $cap, $user_ID, $args ) {
    if ( ( $cap === 'edit_user' || $cap === 'delete_user' ) && $args ) {
        $the_user = get_userdata( $user_ID ); // The user performing the task
        $user     = get_userdata( $args[0] ); // The user being edited/deleted
        
        if ( $the_user && $user && $the_user->ID != $user->ID /* User can always edit self */ ) {
            $allowed = wpse_188863_get_allowed_roles( $the_user );
            
            if ( array_diff( $user->roles, $allowed ) ) {
                // Target user has roles outside of our limits
                $caps[] = 'not_allowed';
            }
        }
    }
    
    return $caps;
}

add_filter( 'map_meta_cap', 'wpse_188863_map_meta_cap', 10, 4 );

/* ###################################################################################
 * On filtre l'affichage de la barre d'administration Wordpress (noir)
 * ################################################################################### */

function filterAdminBarRender() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('customize');
    $wp_admin_bar->remove_menu('wpseo-menu');
    $wp_admin_bar->remove_menu('new_draft');
    $wp_admin_bar->remove_menu('wp-logo');
}
add_action( 'wp_before_admin_bar_render', 'filterAdminBarRender', 100 );

/* ###################################################################################
 * On supprime le support des commentaires pour les pages
 * ################################################################################### */

add_action('init', 'removeCommentsSupport', 100);

function removeCommentsSupport() {
    remove_post_type_support( 'page', 'comments' );
}



/* ###################################################################################
 * On ajoute un lien vers la documentation dans la barre d'administration
 * ################################################################################### */

function setDocumentationExternalLink($admin_bar)  {
    $args = array(
        'id'        => 'hwc-documentation', // Must be a unique name
        'title'     => '<style>#wp-admin-bar-hwc-documentation .ab-icon:before{content:"\f223";top:3px;};</style><span class="ab-icon"></span>Documentation', // Label for this item
        'href'      =>__ ('https://drive.google.com/drive/folders/1dGt-9sy0u7Wk9GPCBZKva6c9msNG28fa?usp=sharing'),
        'meta'  => array(
            'target'=> '_blank', // Opens the link with a new tab
            'title' => __('Documentation'), // Text will be shown on hovering
        ),
    );
    $admin_bar->add_menu( $args);
}
add_action('admin_bar_menu', 'setDocumentationExternalLink', 101); 

<?php

/* ###################################################################################
 * Desactive certains maj de plugins
 * ################################################################################### */

function setFilterToPluginUpdates( $value ) {
	$plugins = array(
	       /*'white-label-cms/wlcms-plugin.php',
			'admin-columns-pro/admin-columns-pro.php',
			'admin-menu-editor-pro/menu-editor.php',
			'wp-media-folder/wp-media-folder.php',*/
	);
	foreach ($plugins as $plugin):
    	if(isset($value->response[$plugin])):
    	   unset( $value->response[$plugin] );
    	endif;
	endforeach;
	return $value;
}
add_filter( 'site_transient_update_plugins', 'setFilterToPluginUpdates' );


/* ###################################################################################
 * Ajoute des formats au tinymce
 * ################################################################################### */

function setTinymceStyleFormats( $init_array ) {

	$style_formats = array(
	    // Each array child is a format with it's own settings
	    array(
	        'title' => 'Boutons',
	        'items' => array(
	            array(
	                'title' => 'Bouton',
	                'selector' => 'a',
	                'classes' => 'btn default',
	                'wrapper' => false,
	            ),
	            array(
	                'title' => 'Bouton lien',
	                'selector' => 'a',
	                'classes' => 'btn link',
	                'wrapper' => false,
	            ),
	        )
	    ),
	    array(
	        'title' => 'Couleurs',
	        'items' => array(
	            array(
	                'title' => 'Couleur principale',
	                'inline' => 'span',
	                'classes' => 'color-1',
	                'wrapper' => true,
	            ),
	            array(
	                'title' => 'Gris',
	                'inline' => 'span',
	                'classes' => 'color-grey',
	                'wrapper' => true,
	            ),
	        )
	    ),
	    array(
	        'title' => 'Mise en forme',
	        'items' => array(
	            array(
	                'title' => 'Majuscules',
	                'inline' => 'span',
	                'classes' => 'style-uppercase',
	                'wrapper' => true,
	            ),
	        )
	    ),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'setTinymceStyleFormats' );

/* ###################################################################################
 * Filtre les formats html et taille de typos
 * ################################################################################### */

function setTinymceBlockFormats($arr){
    $arr['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4';
    return $arr;
  }
add_filter('tiny_mce_before_init', 'setTinymceBlockFormats');

/* ###################################################################################
 * Permet de conserver les modifs sur les spans dans tinymce
 * ################################################################################### */

function setAllowedTagsForTinymce($init) {
	// Command separated string of extended elements
	$ext = 'span[id|name|class|style]';

	// Add to extended_valid_elements if it alreay exists
	if ( isset( $init['extended_valid_elements'] ) ) {
		$init['extended_valid_elements'] .= ',' . $ext;
	} else {
		$init['extended_valid_elements'] = $ext;
	}

	return $init;
}

add_filter('tiny_mce_before_init', 'setAllowedTagsForTinymce' );

/* ###################################################################################
 * Init des pages d'options ACF
 * ################################################################################### */

add_action('init', 'setAcfOptionsPages');

function setAcfOptionsPages() {

	if( function_exists('acf_add_options_page') ) {
		$main_slug = 'config-main';
		acf_add_options_page(array(
				'page_title' 	=> 'Configuration',
				'menu_title'	=> 'Configuration',
				'menu_slug' 	=> $main_slug,
				'capability'	=> 'edit_posts',
				'parent_slug'	=> '',
				'position'		=> '2.001',
				'icon_url'		=> 'dashicons-admin-tools',
				'redirect'		=> true,
				'post_id'		=> 'options',
				'autoload'		=> false,
				'update_button'	=> __('Mise à jour', 'wpb')
		));
		acf_add_options_page(array(
				'page_title' 	=> 'Construction',
				'menu_title'	=> 'Construction',
				'menu_slug' 	=> 'config-build',
				'capability'	=> 'setup_network',
				'parent_slug'	=> '',
				'position'		=> '2.002',
				'icon_url'		=> 'dashicons-admin-tools',
				'redirect'		=> true,
				'post_id'		=> 'options',
				'autoload'		=> false,
				'update_button'	=> __('Mise à jour', 'wpb')
		));
		$current_user = wp_get_current_user();
		$sub_options_pages = array(
				array('name' => 'Général', 'slug' => 'general'),
				array('name' => 'Contact', 'slug' => 'contact'),
		);
		$all_langs = pll_languages_list();
		foreach ($all_langs as $lang):
		$sub_options_pages[] = array('name' => 'Contenus '.$lang, 'slug' => 'content-'.$lang);
		endforeach;

		if($current_user->user_login == 'hwc'):
		$sub_options_pages[] = array('name' => 'Actualités', 'slug' => 'posts');
		$sub_options_pages[] = array('name' => 'HWC', 'slug' => 'hwc');
		endif;
		$i = 0;
		foreach ($sub_options_pages as $subpages):
			acf_add_options_sub_page(array(
					'page_title' 	=> $subpages['name'],
					'menu_title'	=> $subpages['name'],
					'menu_slug' 	=> 'config-'.$subpages['slug'],
					'capability'	=> 'edit_posts',
					'parent_slug'	=> $main_slug,
					'position'		=> $i,
					'icon_url'		=> false,
					'redirect'		=> false,
					'post_id'		=> 'config-'.$subpages['slug'],
					'autoload'		=> false,
					'update_button'	=> __('Mise à jour', 'wpb')
			));
			$i++;
		endforeach;

	}

}

/* ###################################################################################
 * Ajout de block ACF pour Gutenberg
 * ################################################################################### */

function setGutenbergBlocksCategories( $categories, $block_editor_context ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'hwc',
				'title' => __( 'Principaux', 'wpb' ),
				'icon'  => '',
			),
		    array(
		        'slug' => 'hwc-medias',
		        'title' => __( 'Médias', 'wpb' ),
		        'icon'  => '',
		    ),
		    array(
		        'slug' => 'hwc-misc',
		        'title' => __( 'Divers', 'wpb' ),
		        'icon'  => '',
		    ),
		    array(
		        'slug' => 'hwc-special',
		        'title' => __( 'Uniques', 'wpb' ),
		        'icon'  => '',
		    ),
		)
	);
}
add_filter( 'block_categories_all', 'setGutenbergBlocksCategories', 10, 2 );

add_action('acf/init', 'setAcfBlocksInitForGutenberg');
function setAcfBlocksInitForGutenberg() {
	// check function exists
	if( function_exists('acf_register_block') ) {

		$guts = new WP_Query(array(
			'post_type' => 'hwc-gutenberg',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'meta_key' => 'title',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		));
		if($guts->have_posts()):
			while ($guts->have_posts()):
				$guts->the_post();
				$keywords = array();
				$acf_keywords = get_field('keywords');
				if($acf_keywords):
					foreach ($acf_keywords as $k):
						$keywords[] = $k['keyword'];
					endforeach;
				endif;
				$post_types = array();
				$acf_post_types = get_field('post_types');
				if($acf_post_types):
					foreach ($acf_post_types as $pt):
						$post_types[] = $k['post_type'];
					endforeach;
				endif;

				$acf_block_args = array(
				    'name'				=> get_field('name'),
				    'title'				=> get_field('title'),
				    'description'		=> get_field('description'),
				    'render_callback'	=> 'setAcfBlocksRenderForGutenberg',
				    'category'			=> get_field('category'),
				    'icon'				=> get_field('icon'),
				    'keywords'			=> $keywords,
				    'mode' 				=> get_field('mode'),
				    'post_types' 		=> $post_types,
				    'supports'			=> array(
				        'align' => false,
				        'mode' => true,
				        'multiple' => true,
				    ),
				    'enqueue_assets' => 'setAcfBlocksEnqueueAssetsForGutenberg'
				);
				/*$acf_block_args['example'] = array(
				    'attributes' => array(
				        'mode' => 'preview',
				        'data' => array(
                            'text' => 'Exemple',
                            '_text' => 'Exemple',
				        ),
				    ));*/
				acf_register_block($acf_block_args);
			endwhile;
			wp_reset_postdata();
		endif;

	}
}

/* ###################################################################################
 * Affichage du builder
 * ################################################################################### */

function setAcfBlocksRenderForGutenberg( $block ) {

	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	if( file_exists( get_theme_file_path("/builder/{$slug}.php") ) ):
		$pi_classes = $slug;
		if(get_field('show_cta')):
			$pi_classes .= ' has-column';
		endif;
		echo "<div class='page-item ".$pi_classes."'>";
			include( get_theme_file_path("/builder/{$slug}.php") );
		echo "</div>";
	else:
		echo "
		<div class='page-item text'>
			<div class='container'>
				<div class='text'><p class='alert error'><strong>[Alerte dev]</strong> Le fichier requis n'existe pas dans : ".get_theme_file_path("/builder/{$slug}.php")." </p></div>
			</div>
		</div>";
	endif;
}

/* ###################################################################################
 * Enregistrement des CSS et JS du block
 * ################################################################################### */

function setAcfBlocksEnqueueAssetsForGutenberg($block_type) {

    $name = str_replace('acf/', '', $block_type['name']);

    if(!is_admin()): // Front
        $block_assets = get_theme_file_path("/builder/assets/front/{$name}.php");
    	if(file_exists($block_assets)):
    		include($block_assets);
    	endif;
	else: // Back office
    	$block_assets = get_theme_file_path("/builder/assets/back/{$name}.php");
    	if(file_exists($block_assets)):
    	   include($block_assets);
    	endif;
	endif;
}


/* ###################################################################################
 * Désactivation de Gutenberg en fonction du type de page ou template
 * ################################################################################### */


function setConditionsForDisabledEditor( $id = false ) {

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$cur_post = get_post($id);

	$excluded_post_types = get_field('disable_gutenberg_post_types', 'config-hwc');
	if(!$excluded_post_types || empty($excluded_post_types)):
		$excluded_post_types = array();
	endif;

	$excluded_templates = get_field('disable_gutenberg_page_templates', 'config-hwc');
	if(!$excluded_templates || empty($excluded_templates)):
		$excluded_templates = array();
	endif;

	$excluded_type = get_field('disable_gutenberg_page_types', 'config-hwc');
	$excluded_ids = array();
	if($excluded_type):
		foreach ($excluded_type as $type):
			if($type == 'front_page'):
				if(pll_get_post(get_option('page_on_front')) == $id):
					$excluded_ids[] = $id;
				endif;
			endif;
			if($type == 'home'):
				if(pll_get_post(get_option('page_for_posts')) == $id):
					$excluded_ids[] = $id;
				endif;
			endif;
		endforeach;
	endif;
	$template = get_page_template_slug( $id );
	$post_type = $cur_post->post_type;

	return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates ) || in_array( $post_type, $excluded_post_types );
}

function hasPostTypeGutenbergDisabled($post_type = 'post') {
    $excluded_post_types = get_field('disable_gutenberg_post_types', 'config-hwc');
    if(!$excluded_post_types || empty($excluded_post_types)):
        $excluded_post_types = array();
    endif;
    return in_array( $post_type, $excluded_post_types );
}

function setConditionsForDisabledGutenberg( $can_edit, $post_type ) {
    if(empty( $_GET['post'] )): // Si on est sur la page de création d'un nouveau post, l'id n'est pas défini
        $excluded_post_types = get_field('disable_gutenberg_post_types', 'config-hwc');
        if(is_array($excluded_post_types) && in_array( $post_type, $excluded_post_types )): // On vérifie alors que les posts n'ont pas gutenberg de désactivé
            $can_edit = false;
        endif;
    endif;
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( setConditionsForDisabledEditor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;

}
add_filter( 'gutenberg_can_edit_post_type', 'setConditionsForDisabledGutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'setConditionsForDisabledGutenberg', 10, 2 );


/* ###################################################################################
 * Blocs Gutenberg autorisés
 * ################################################################################### */

add_filter( 'allowed_block_types_all', 'setAllowedGutenbergBlockTypes', 10, 2);
function setAllowedGutenbergBlockTypes( $allowed_blocks, $block_editor_context ) {
    // Il est nécessaire de conserver $post pour ne pas casser la boucle.
    global $post;
    $post_backup = $block_editor_context->post;
	$allowed_blocks = array();
	$guts = new WP_Query(array(
		'post_type' => 'hwc-gutenberg',
		'posts_per_page' => -1,
	));
	if($guts->have_posts()):
		while ($guts->have_posts()):
			$guts->the_post();
			$allowed_blocks[] = 'acf/'.get_field('name', get_the_ID());
		endwhile;
		wp_reset_postdata();
	endif;
	$post = $post_backup;
	return $allowed_blocks;

}

/* ###################################################################################
 * On ne charge Recaptcha de CF7 que sur les pages utiles
 * ################################################################################### */

add_action('wp_print_scripts', 'setRecaptchaScriptLoading', 11, 0);

function setRecaptchaScriptLoading() {
    $allowed_pages = array(pll_get_post(10)); // 10 = Page contact
    if ( !in_array(get_the_ID(), $allowed_pages) ):
        wp_dequeue_script( 'google-recaptcha' );
        wp_dequeue_script( 'wpcf7-recaptcha' );
    endif;
}

/* ###################################################################################
 * On test si le plugin Woocommerce est activé
 * ################################################################################### */

if ( ! function_exists( 'is_woocommerce_activated' ) ) {
    function is_woocommerce_activated() {
        if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
    }
}

/* ###################################################################################
 * On réecrit le body des messages envoyés par Contact form 7 pour incorporer 
 * le template de mail
 * ################################################################################### */

add_filter( 'wpcf7_mail_components', 'setCF7MailTemplate',10,3);
function setCF7MailTemplate($components, $form, $mailobj){
    // @todo    
    //$components['body'];
    return $components;
}

/**
 * Disables the Site Reviews stylesheet.
 * Paste this in your active theme's functions.php file.
 * @return bool
 */
//add_filter('site-reviews/assets/css', '__return_false');

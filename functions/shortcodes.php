<?php
/* ###################################################################################
 * Affichage le drapeau des langues
 * ################################################################################### */

function shortcode_hwclang( $atts ){

    if(isset($atts['dna'])):
        $dna = $atts['dna'];
    else:
        $dna = 'slug';
    endif;

    if(isset($atts['sf'])):
        $sf = $atts['sf'];
    else:
        $sf = 0;
    endif;

    ob_start();
	echo "<ul class='langs'>";
	$args_l = array('show_flags'=> $sf, 'display_names_as' => $dna, 'hide_if_empty' => 0);
	pll_the_languages($args_l);
	echo "</ul>";
	return(ob_get_clean());
}
add_shortcode( 'hwclang', 'shortcode_hwclang' );

/* ###################################################################################
 * Affiche une carte google map selon les parametres:
 * acf_page_id : identifiant de la page contenant le repeater
 * acf_id : id de l'acf sur la page
 * div_id : id de la div container pour la sortie
 * ################################################################################### */

function shortcode_hwcgmap( $atts ){
    if($atts['acf_page_id'] == 'home'):
        $page_id = get_option('page_on_front');
    elseif($atts['acf_page_id'] == 'posts'):
        $page_id = get_option('page_for_posts');
    elseif($atts['acf_page_id'] != ''):
        $page_id = $atts['acf_page_id'];
    else:
        $page_id = get_the_ID();
    endif;
    $markers = get_field($atts['acf_id'], $page_id);
    return displayHwcMap($markers, $atts, false);
}
add_shortcode( 'hwcgmap', 'shortcode_hwcgmap' );


/* ###################################################################################
 * Affiche un menu
 * ################################################################################### */

function shortcode_hwcmenu( $atts ){

	ob_start();
	$tl = $atts['theme_location'];
	wp_nav_menu(array('theme_location' => $tl));
	return(ob_get_clean());

}
add_shortcode( 'hwcmenu', 'shortcode_hwcmenu' );

/* ###################################################################################
 * Affiche le template mailchimp
 * ################################################################################### */

function shortcode_hwcmailchimp( $atts ){
	ob_start();
		include(locate_template('parts/mailchimp.php'));
	return(ob_get_clean());
}
add_shortcode( 'hwcmailchimp', 'shortcode_hwcmailchimp' );

/* ###################################################################################
 * Affiche le template sendinblue
 * ################################################################################### */

function shortcode_hwcsendinblue( $atts ){
    ob_start();
    include(locate_template('parts/sendinblue.php'));
    return(ob_get_clean());
}
add_shortcode( 'hwcsendinblue', 'shortcode_hwcsendinblue' );

/* ###################################################################################
 * Affiche les icones des reseaux sociaux definis
 * ################################################################################### */

function shortcode_hwcsocial( $atts ){
	global $home_id;
	if(!isset($atts['network']) || empty($atts['network'])):
		return;
	endif;
	$fa = $atts['fa'];
	ob_start();


	$network = get_field_object($atts['network'], 'config-contact');
	if(isset($network['value']) && !empty($network['value'])): ?>
		<a href="<?= $network['value']; ?>" target="_blank" rel="noopener" title="<?php _e('Join us on', 'wpb'); echo ' '.$network['label']; ?>" class="social <?= toUrl($network['label']); ?>"><?php if($fa): ?><i class="fab <?= $fa;?>"></i><?php else: ?><span></span><?php endif; ?></a>
	<?php endif;

	return(ob_get_clean());
}
add_shortcode( 'hwcsocial', 'shortcode_hwcsocial' );

/* ###################################################################################
 * Affiche les coordonnées
 * ################################################################################### */

function shortcode_hwccontact( $atts ){
	if(!isset($atts['field']) || empty($atts['field'])):
		return false;
	endif;
	if(!isset($atts['position']) || empty($atts['position'])):
	   $position = 0;
	else:
	   $position = $atts['position'];
	endif;
	$field = $atts['field'];
	$fields = get_field('contacts', 'config-contact');
	$return = $fields[$position][$field];
	if(empty($return)): 
	   return; 
	endif;
	if(isset($atts['linked']) && $atts['linked'] == '1'):
    	if($field == 'e-mail'):
    	    $return = displayEmail($return, false);
    		$return = '<a class="email-js" href="mailto:'.$return.'">'.$return.'</a>';
		elseif(in_array($field, array('phone', 'mobile', 'fax'))):
		    $return = displayEmail($return, false);
	        $return = '<a class="email-js" href="tel:'.$return.'">'.$return.'</a>';
	    endif;
	endif;
	return $return;
}
add_shortcode( 'hwccontact', 'shortcode_hwccontact' );

/* ###################################################################################
 * Affiche le copyright
 * ################################################################################### */

function shortcode_hwccopyright( $atts ){
	$href = get_field('copyright_link', 'config-hwc');
	$title = get_field('copyright_link_title', 'config-hwc');
	$label_type = get_field('copyright_link_label_type', 'config-hwc');
	if($label_type == 'text'):
	$label = get_field('copyright_link_label_text', 'config-hwc');
	elseif($label_type == 'img'):
	   $label_img_type = get_field('copyright_link_label_img_type', 'config-hwc');
	   $label_img_name = get_field('copyright_link_label_img_name', 'config-hwc');
	   if($label_img_type == 'svg'):
	       $label = displaySvg($label_img_name, false);
	   elseif($label_img_type == 'png_jpeg'):
	       $label = '<img src="'.get_stylesheet_directory_uri().'/assets/images/'.$label_img_name.'" />';
	   endif;
	endif;
	return "<a href='".$href."' title='".$title."' target='_blank' rel='noopener'>".$label."</a>";
}
add_shortcode( 'hwccopyright', 'shortcode_hwccopyright' );

/* ###################################################################################
 * Affiche le formulaire de saisie du mot de passe sur les contenus protégés
 * ################################################################################### */

function shortcode_hwcprotectedcontent($atts) {
    return get_the_password_form();
}

add_shortcode( 'hwcprotectedcontent', 'shortcode_hwcprotectedcontent' );
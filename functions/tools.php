<?php

/* ###################################################################################
 * Affichage des variables / functions
 * ################################################################################### */

function p($var, $hide = false)
{
	if($hide):
	   echo "<div style='display:none;'>";
	endif;
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
	if($hide):
	   echo "</div>";
	endif;
}

/* ###################################################################################
 * Convertit une chaine en slug / url
 * ################################################################################### */

function toUrl($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}

/* ###################################################################################
 * Ajoute http:// ou https:// si manquant
 * ################################################################################### */

function addScheme($url, $scheme = 'http://')
{
    return parse_url($url, PHP_URL_SCHEME) === null ?
    $scheme . $url : $url;
}

/* ###################################################################################
 * Recherche si la page en cours est enfant de la page testée. $pid par defaut
 * à définir
 * ################################################################################### */

function isPostChildOfAnother($pid, $parent_id) {
    $anc = get_post_ancestors( $pid );
	foreach($anc as $ancestor) {
	    if($ancestor == $parent_id) {
			return true;
		}
	}
	return false;
}

/* ###################################################################################
 * Retourne une chaine de $pool comprise entre $var1 et $var2.
 * Exemple GetBetween('<span>','</span>','<span>foo</span>') retourne "foo".
 * ################################################################################### */

function GetBetween($var1="",$var2="",$pool){
	$temp1 = strpos($pool,$var1)+strlen($var1);
	if($temp1 === false)
	{
		return false;
	}
	$result = substr($pool,$temp1,strlen($pool));
	$dd=strpos($result,$var2);
	if($dd == 0){
		$dd = strlen($result);
	}

	return substr($result,0,$dd);
}

/* ###################################################################################
 * Retourne une taille de fichier lisible
 * @param string  $file The internal path of the file (not a url)
 * @param boolean $pow  True: power of 10; False: power of 2
 * ################################################################################### */

function getSize($file, $pow = true) {
    $bytes = filesize($file);
    $B = __("B", "wpb");
    $v = $pow?1000:1024;
    $s = $pow?array($B, "K$B", "M$B", "G$B"):array($B, "Ki$B", "Mi$B", "Gi$B");
    $e = floor(log($bytes)/log($v));
    return sprintf('%.2f '.$s[$e], ($bytes/pow($v, floor($e))));
}

/* ###################################################################################
 * Tri de tableau par rapport à une colonne de ce dernier. Exemple:
 * $test = array(
 *  array('name' => 'Pierre', 'positio' => 2),
 *  array('name' => 'Paul', 'position' => 1),
 *  array('name' => 'Jacques', 'position' => 3)
 * );
 * array_sort_by_column($test, 'position); Résultat :
 * $test = array(
 *  array('name' => 'Paul', 'position' => 1),
 *  array('name' => 'Pierre', 'position' => 2),
 *  array('name' => 'Jacques', 'position' => 3)
 * );
 * ################################################################################### */


function sortArrayByColumn(&$arr, $col, $dir = SORT_ASC) {
	$sort_col = array();
	foreach ($arr as $key=> $row) {
		$sort_col[$key] = $row[$col];
	}

	array_multisort($sort_col, $dir, $arr);
	return $arr;
}

/* ###################################################################################
 * Initialisation des ID utiles des pages / posts
 * ################################################################################### */

function setIds() {
	/* Usefull in sidebar */
    global $home_id, $blogpage_id, $cid, $parent_id, $post, $protected_id;

	if($post):
    	if ($post->post_parent):
    		$ancestors=get_post_ancestors($post->ID);
    		$root=count($ancestors)-1;
    		$parent_id = $ancestors[$root];
    	else:
    		$parent_id = $post->ID;
    	endif;
    	$parent_id = pll_get_post($parent_id);
	endif;

	$home_id = get_option('page_on_front');
	$blogpage_id = get_option( 'page_for_posts' );
	$protected_id = get_field('protected', 'config-hwc');

	if(!$cid):
    	if(is_home() || is_category()):
    	$cid = $blogpage_id;
    	elseif(is_archive('product') && is_woocommerce_activated()):
    	$cid = wc_get_page_id('shop');
    	elseif(is_search()):
    	$cid = get_field('search', 'config-hwc');
    	elseif(is_404()):
    	$cid = get_field('404', 'config-hwc');
    	elseif(is_page() || is_single()):
    	$cid = get_the_ID();
    	elseif(!isset($cid) || !$cid):
    	$cid = $home_id;
    	endif;
    	$cid = pll_get_post($cid);
	endif;
	$home_id = pll_get_post($home_id);
	$blogpage_id= pll_get_post($blogpage_id);
	$protected_id= pll_get_post($protected_id);
}

/* ###################################################################################
 * Télécharge un média à partir d'une url et l'ajoute à la bibliothéque de medias
 * ################################################################################### */

function ImportAttachmentFromUrl($url, $is_http = true, $parent_post_id = null) {
    if($is_http):
        if( !class_exists( 'WP_Http' ) ):
		  include_once( ABSPATH . WPINC . '/class-http.php' );
        endif;
		$http = new WP_Http();
		$response = $http->request( $url );
		if( $response['response']['code'] != 200 ):
			return false;
		endif;
	else:
	  $response = array('body' => file_get_contents($url["tmp_name"]));
	  $url = $url["name"];
	endif;
	$upload = wp_upload_bits( basename($url), null, $response['body'] );
	if( !empty( $upload['error'] ) ) {
		return false;
	}
	$file_path = $upload['file'];
	$file_name = basename( $file_path );
	$file_type = wp_check_filetype( $file_name, null );
	$attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
	$wp_upload_dir = wp_upload_dir();
	$post_info = array(
			'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
			'post_mime_type' => $file_type['type'],
			'post_title'     => $attachment_title,
			'post_content'   => '',
			'post_status'    => 'inherit',
	);
	// Create the attachment
	$attach_id = wp_insert_attachment( $post_info, $file_path, $parent_post_id );
	// Include image.php
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	// Define attachment metadata
	$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
	// Assign metadata to attachment
	wp_update_attachment_metadata( $attach_id,  $attach_data );
	return $attach_id;
}

/* ###################################################################################
 * On définit tous les médias qui sont sans langue dans la langue par défaut
 * A lancer une fois si la traduction des médias de Polylang
 * a été activé après l'insertion des médias
 * ################################################################################### */

//add_action('init', 'fixExistingMediaToDefaultLanguage');

function fixExistingMediaToDefaultLanguage() {
    $args = array(
        'post_type'=> 'attachment',
        'post_status'    => 'all',
        'posts_per_page' => -1, // Make me less if PHP can't handle it
        'lang' => ''
    );

    $att_query = new WP_Query( $args );
    if($att_query->have_posts()):
        while ($att_query->have_posts()):
            $att_query->the_post();
            $lang = pll_get_post_language(get_the_ID());
            if($lang === false):
                pll_set_post_language(get_the_ID(), pll_default_language());
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
}

/* ###################################################################################
 * Retourne la bonne url en fonction des différents filtres actifs
 * ################################################################################### */

function getUrlForFilters($base_url, $args_to_add = array()) {
    $url_args = array_merge( $_GET, $args_to_add );
    $url_args = array_filter($url_args); // Suppression des entrées sans valeur. Ex : Retour vers "toutes les catégories"
    if(empty($url_args)):
        return $base_url;
    endif;
    return $base_url.'?'.http_build_query( $url_args );
}

/* ###################################################################################
 * Inscription Mailchimp par API
 * ################################################################################### */

function registerMailToMailchimpList($name, $mail, $phone) {
    $list_id = 'xxx';
    $us_id = 'xxx';
    $authToken = 'xxx-'.$us_id;
    // The data to send to the API
    
    $postData = array(
        "email_address" => $mail,
        "status" => "subscribed",
        "merge_fields" => array(
            "FNAME"=> $name,
            "PHONE"=> $phone
        )
    );
    
    // Setup cURL
    $ch = curl_init('https://'.$us_id.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey '.$authToken,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    // Send the request
    $response = curl_exec($ch);
}

/* ###################################################################################
 * Modifie le champ class d'une chaine html
 * ################################################################################### */

function replaceClassAttrValue($html, $remove, $add) {
    $classes = GetBetween('class="', '"', $html); // On récupere le contenu de l'attribut class=""
    $newclasses = $classes.' '.$add; // On rajoute nos classes
    $newclasses = str_replace($remove, '', $newclasses); // On supprime les classes
    return str_replace('class="'.$classes.'"', 'class="'.trim($newclasses).'"', $html);
}

function addClass($htmlString = '', $newClass) {
    $pattern = '/class="([^"]*)"/';
    
    // class attribute set
    if (preg_match($pattern, $htmlString, $matches)) {
        $definedClasses = explode(' ', $matches[1]);
        if (!in_array($newClass, $definedClasses)) {
            $definedClasses[] = $newClass;
            $htmlString = str_replace($matches[0], sprintf('class="%s"', implode(' ', $definedClasses)), $htmlString);
        }
    }
    
    // class attribute not set
    else {
        $htmlString = preg_replace('/(\<.+\s)/', sprintf('$1class="%s" ', $newClass), $htmlString);
    }
    
    return $htmlString;
}


/* ###################################################################################
 * Récupére les champs ACF d'un bloc donné d'un post donné
 * $source_id = id du post ciblé par la récupération des champs
 * $block_id = identifiant gutenberg du champs. Ex: top-banner 
 * $limit = nombre de résultats à retourner maximum
 * ################################################################################### */

function getFieldsFromPost($source_id, $block_id, $limit = 1) {
    $source = get_post($source_id);
    $sourceBlocks = parse_blocks($source->post_content);
    $data = array();
    $i=1;
    foreach ($sourceBlocks as $block):
        if($block['blockName'] == 'acf/'.$block_id):
            acf_setup_meta( $block['attrs']['data'], $source_id+$i); // On formate les champs pour avoir un format attendu et non brut.
            $data[] = get_fields($source_id+$i);
            acf_reset_meta($source_id+$i);
            if($limit !== false && $limit == $i):
                if($limit == 1):
                    return $data[0];
                else:
                    return $data;
                endif;
            endif;
            $i++;
        endif;
        
    endforeach;
    return $data;
}

/* ###################################################################################
 * Retourne le terme principale d'une taxo en se servant de la fonctionnalité de Yoast
 * ################################################################################### */

function getPrimaryTerm($taxo, $post_id) {
    if ( class_exists('WPSEO_Primary_Term') ):
    $wpseo_primary_term = new WPSEO_Primary_Term( $taxo, $post_id );
    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
    $term = get_term( $wpseo_primary_term );
    if ( !is_wp_error( $term ) ):
    return $term;
    endif;
    endif;
    return false;
}

/* ###################################################################################
 * Défini le terme principal d'une taxo
 * ################################################################################### */

function setPrimaryTerm($taxonomy, $postID, $term){
    if ( class_exists('WPSEO_Primary_Term') ) {
        // Set primary term.
        $primaryTermObject = new WPSEO_Primary_Term($taxonomy, $postID);
        $primaryTermObject->set_primary_term($term);
    }
}
<?php

include 'woocommerce/styles.php';
include 'woocommerce/theme.php';

function displayProductScore($product_id) {
    $stars = '<span class="product-stars">';
    $score = getProductScore($product_id);
    if($score['count'] > 0):
        $result = $score['rating'];
        $stars_count = 1;
        while ($result > 1):
            $stars .= '<span><span style="width:100%"></span></span>';
            $result--;
            $stars_count++;
        endwhile;
        if($result > 0):
            $stars .= '<span><span style="width:'.($result*100).'%"></span></span>';
        endif;
        while($stars_count < 5):
            $stars .= '<span><span style="width:0%"></span></span>';
            $stars_count++;
        endwhile;
    endif;  
    $stars .= '</span>';
    $translation_count = $score['count'];
    if($translation_count == 0):
        $translation_count = 1; // Fix translation zero = plural
    endif;  
    $permalink = get_permalink($product_id);
    if(is_singular('product') && $product_id == get_the_ID()):
        $permalink = '';
    endif;
    $html = $stars.'<a href="'.$permalink.'#reviews" class="count">';
    if($score['count'] > 0):
        $html .= sprintf( _n( '1 note', '%s notes', $translation_count, 'wpb' ), number_format_i18n( $score['count'] ) );
    else:
    $html .= __('Non noté', 'wpb');
    endif;
    $html .= '</a>';
    echo $html;
}

function getProductScore($product_id) {
    $ratingInfo = glsr_get_ratings([
        'assigned_posts' => $product_id,
    ]);
    return array('count' => $ratingInfo['reviews'], 'rating' => $ratingInfo['average']);
}






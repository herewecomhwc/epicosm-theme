<?php 

/* ###################################################################################
 * Modifie les classes des boutons ajouter au panier
 * ################################################################################### */

function woocommerce_loop_add_to_cart_link_hwc($output, $product, $args) {
    return replaceClassAttrValue($output, 'button', 'btn default');
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'woocommerce_loop_add_to_cart_link_hwc', 10, 3 );

/* ###################################################################################
 * Modifie les classes du bouton de commande
 * ################################################################################### */

function woocommerce_order_button_html_hwc($html) {
    return replaceClassAttrValue($html, 'button alt', 'btn default');
}

add_filter( 'woocommerce_order_button_html', 'woocommerce_order_button_html_hwc');

/* ###################################################################################
 * Modifie les classes des H2 des loops produits
 * ################################################################################### */

add_filter( 'woocommerce_product_loop_title_classes', 'woocommerce_product_loop_title_classes_hwc', 10, 1 );
function woocommerce_product_loop_title_classes_hwc( $class ) {
    $class .= ' h3';
    return $class;
}

/* ###################################################################################
 * Modifier les classes du bouton "Voir le panier" dans la notification d'ajout au
 * panier
 * ################################################################################### */

add_filter( 'wc_add_to_cart_message_html', 'wc_add_to_cart_message_html_hwc', 10, 3 );
function wc_add_to_cart_message_html_hwc(  $message, $products, $show_qty ) {
    return replaceClassAttrValue($message, 'button wc-forward', 'btn default');
}

/* ###################################################################################
 * Encapsule les sorties Woocommerce dans des div .page-item
 * ################################################################################### */

function woocommerce_shortcode_before_products_loop_hwc( $attr ) {
    echo '<div class="page-item woocommerce-products"><div class="container">';
}

function woocommerce_shortcode_after_products_loop_hwc( $attr ) {
    echo '</div></div>';
}

add_action( 'woocommerce_shortcode_before_products_loop', 'woocommerce_shortcode_before_products_loop_hwc' );
add_action( 'woocommerce_shortcode_after_products_loop', 'woocommerce_shortcode_after_products_loop_hwc' );

/* ###################################################################################
 * Force un aspect responsive sur les images de la galerie produit
 * ################################################################################### */

function woocommerce_single_product_image_thumbnail_html_hwc($html, $id) {
    return str_replace('<a href=', '<a class="ir ir-square" href=', $html);
}

//add_filter( 'woocommerce_single_product_image_thumbnail_html', 'woocommerce_single_product_image_thumbnail_html_hwc', 10, 2);

/* ###################################################################################
 * Modifie les classes du bouton d'ajout au panier des blocks Gutenberg Woocommerce
 * ################################################################################### */

function woocommerce_blocks_product_grid_item_html_hwc($html, $data, $product) {
    $data->button = str_replace('wp-block-button__link add_to_cart_button', 'wp-block-button__link add_to_cart_button btn default', $data->button);
    return "<li class=\"wc-block-grid__product\">
				<a href=\"{$data->permalink}\" class=\"wc-block-grid__product-link\">
					{$data->image}
					{$data->title}
				</a>
				{$data->badge}
				{$data->price}
				{$data->rating}
				{$data->button}
			</li>";
}

add_filter( 'woocommerce_blocks_product_grid_item_html', 'woocommerce_blocks_product_grid_item_html_hwc', 10, 3);
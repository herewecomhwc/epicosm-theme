<?php

/* ###################################################################################
 * Nombre de commandes par page dans la liste des commandes du compte client
 * ################################################################################### */

add_filter( 'woocommerce_my_account_my_orders_query', 'woocommerce_my_account_my_orders_query_hwc', 10, 1 );
function woocommerce_my_account_my_orders_query_hwc( $args ) {
    $args['limit'] = 15;
    return $args;
}

/* ###################################################################################
 * On enregistre la sidebar Woocommerce
 * ################################################################################### */

function add_woocommerce_sidebar() {
    register_sidebar( array(
        'name'          => __( 'Sidebar boutique', 'wpb' ),
        'id'            => 'shop-sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="widgettitle h4">',
        'after_title'   => '</div>',
    ) );
}
add_action( 'widgets_init', 'add_woocommerce_sidebar' );

/* ###################################################################################
 * Setup theme
 * ################################################################################### */

function setup_woocommerce_theme_hwc() {
    add_theme_support( 'woocommerce', array(
        'gallery_thumbnail_image_width' => 200, // Taille des miniatures de la galerie
        'single_image_width'    => 800, // Taille de l'image principale de la galerie sans effet de zoom
        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 1,
            'max_columns'     => 4,
        ),
    ) );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_setup_theme', 'setup_woocommerce_theme_hwc' );

/* ###################################################################################
 * Définit la taille de l'image utilisée dans le zoom de la galerie
 * ################################################################################### */

function woocommerce_product_thumbnails_large_size_hwc() {
    return 'square_@1x';
}

add_filter( 'woocommerce_product_thumbnails_large_size', 'woocommerce_product_thumbnails_large_size_hwc');

/* ###################################################################################
 * Désactivation des etiquettes Woocommerce
 * ################################################################################### */

function unregister_tags_for_products() {
    unregister_taxonomy_for_object_type( 'product_tag', 'product' );
}
function remove_product_tags_column( $product_columns ) {
    unset( $product_columns['product_tag'] );
    return $product_columns;
}
add_action( 'init', 'unregister_tags_for_products' );
add_filter('manage_product_posts_columns', 'remove_product_tags_column', 999);


/* ###################################################################################
 * On ajoute les blocs Gutenberg Woocommerce autorisés après les blocs ACF custom
 * ################################################################################### */

function getAllowedGutenbergWoocommerceBlockTypes() {
    //return array('woocommerce/handpicked-products', 'woocommerce/product-category', 'woocommerce/product-new', 'woocommerce/product-best-sellers');
    return array();
}

add_filter( 'allowed_block_types_all', 'setAllowedGutenbergWoocommerceBlockTypes', 11, 2);
function setAllowedGutenbergWoocommerceBlockTypes( $allowed_blocks, $block_editor_context ) {
    $woocommerce_blocks_ok = getAllowedGutenbergWoocommerceBlockTypes();
    return array_merge($allowed_blocks, $woocommerce_blocks_ok);
    
}

add_filter( 'render_block', 'wrap_table_block', 10, 2 );
function wrap_table_block( $block_content, $block ) {
    $woocommerce_blocks_ok = getAllowedGutenbergWoocommerceBlockTypes();
    if ( in_array($block['blockName'], $woocommerce_blocks_ok) ):
        $block_content = '<div class="page-item woocommerce-gutenberg-block '.str_replace('woocommerce/', '', $block['blockName']).'"><div class="container">' . $block_content . '</div></div>';
    endif;
    return $block_content;
}

/* ###################################################################################
 * On ne laisse que les produits simples
 * ################################################################################### */

function woocomeerce_allowed_post_types($items) {
    unset( $items['grouped'] );
    //unset( $items['variable'] );
    unset( $items['external'] );
    return $items;
}
add_filter('product_type_selector', 'woocomeerce_allowed_post_types', 10, 1);

/* ###################################################################################
 * On ajout des onglets produits
 * ################################################################################### */

add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
    
    // Adds the new tab
    if(get_field('ingredients', get_the_ID())):
        $tabs['ingredients'] = array(
            'title' 	=> __( 'Ingrédients', 'woocommerce' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_ingredients_tab_content'
        );
    endif;
    if(get_field('how_to_use', get_the_ID())):
        $tabs['how_to_use'] = array(
            'title' 	=> __( 'Utilisation', 'woocommerce' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_how_to_use_tab_content'
        );
    endif;
    
    if(get_field('benefaction', get_the_ID())):
        $tabs['benefaction'] = array(
            'title' 	=> __( 'Bienfaits', 'woocommerce' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_benefaction_tab_content'
        );
    endif;
    
    unset( $tabs['reviews'] );
    
    return $tabs;
    
}
function woo_ingredients_tab_content() {
    
    // The new tab content
    $ingredients = get_field('ingredients', get_the_ID());
    if($ingredients):
        echo '<div class="block-title"><h2 class="fade to-right animate">'.__('Ingrédients', 'wpb').'</h2></div>';
        echo $ingredients;
    endif;
    
}

function woo_how_to_use_tab_content() {
    
    // The new tab content
    $how_to_use = get_field('how_to_use', get_the_ID());
    if($how_to_use):
    echo '<div class="block-title"><h2 class="fade to-right animate">'.__('Utilisation', 'wpb').'</h2></div>';
    echo $how_to_use;
    endif;
    
}

function woo_benefaction_tab_content() {
    
    // The new tab content
    $benefaction = get_field('benefaction', get_the_ID());
    if($benefaction):
    echo '<div class="block-title"><h2 class="fade to-right animate">'.__('Bienfaits', 'wpb').'</h2></div>';
    echo $benefaction;
    endif;
    
}

add_filter('wc_add_to_cart_message_html', 'customizeAddToCartNotification', 10, 3);
function customizeAddToCartNotification($message, $products, $show_qty ) {
    $html = '<div class="atc-notification"><div class="close" data-onclick=\'[{"class":"close","action":"addClass","target":".atc-notification"}]\'></div>';
    $html .= '<div class="head">'.sprintf( __( '%d article(s) ajouté(s) à votre panier', 'wpb' ), count($products) ).'</div>';
    $html .= '<div class="products">';
    foreach($products as $product_id => $qty):
    $wc_product = wc_get_product($product_id);
    $html .= '<div class="product">';
    $html .= '<div class="left">'.getImage('portrait_@1x', get_post_thumbnail_id($product_id)).'</div>';
    $html .= '<div class="right">
    <div class="title">'.get_the_title($product_id).'</div>
    <div class="price">'.$wc_product->get_price_html().'</div>
    <div class="qty"><span>'.__('Quantité', 'wpb').':</span> '.$qty.'</div>
    </div>';
    $html .= '</div>';
    endforeach;
    $html .= '</div>';
    $html .= '<a href="'.wc_get_cart_url().'" class="btn default">'.__('Voir mon panier', 'wpb').'</a></div>';
    return $html;
}

add_filter( 'woocommerce_email_attachments', 'bbloomer_attach_pdf_to_emails', 10, 4 );

function bbloomer_attach_pdf_to_emails( $attachments, $email_id, $order, $email ) {
    $email_ids = array( 
	//'new_order', 
	'customer_processing_order' 
	);
    if ( in_array ( $email_id, $email_ids ) ) {
        $cgv = get_field('woocommerce_cgv', 'config-content-'.pll_current_language());
        if($cgv):
            $path = get_attached_file($cgv['ID']);
            if(!empty($path)):
                $attachments[] = $path;
            endif;
        endif;
    }
    return $attachments;
}
<?php
setIds();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?= esc_url( get_template_directory_uri() ); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">
	function defer(method) {
	    if (window.jQuery)
	        method();
	    else
	        setTimeout(function() { defer(method) }, 50);
	}
	</script>
	<?php wp_head(); ?>
	<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Belleza&family=Roboto:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet"> 
</head>
<body <?php body_class(); ?>>
<?php 
if(is_front_page()):
    $logo_site = get_field('logo_white', 'config-general');
else:
    $logo_site = get_field('logo_black', 'config-general');
endif;  
$logo_mobile = get_field('logo_black', 'config-general');
global $woocommerce;
?>
<div id="page" class="site">
	<div id="header-container">
		<header id="masthead" class="site-header" role="banner">
			<div id="site-logo">
				<a href="<?= get_bloginfo('url'); ?>" title="<?php _e("Back to home", "wpb"); ?>">
					<img src="<?= $logo_site['url']; ?>" alt="<?= $logo_site['alt']; ?>" class="no-lazy"/>
					<img src="<?= $logo_mobile['url']; ?>" alt="<?= $logo_mobile['alt']; ?>" class="no-lazy mobile"/>
				</a>
			</div>
			<span id="toggle-menu" data-onclick='[{"class":"menu-open","action":"toggleClass","target":"#masthead, html, body"}]'><span class="content"></span></span>	
			<nav id="main-menu" class="menu">
				<?php
					wp_nav_menu(array('theme_location' => 'primary', 'walker' => new HWC_Walker()));
			    ?>
			</nav>
			<nav id="secondary-menu">
				<ul>
					<li class="contact"><a href="<?=get_permalink(get_field('contact_page', 'config-content-'.pll_current_language()));?>"><?=__('Contact', 'wpb');?></a></li>
					<li class="langs" style="display:none;"><?= pll_current_language(); ?><?=do_shortcode('[hwclang]');?></li>
					<li class="account"><a href="<?=get_permalink( get_option('woocommerce_myaccount_page_id') );?>"><?php displaySvg('account-icon.svg');?> <span><?=__('Compte', 'wpb');?></span></a></li>
					<li class="cart"><a href="<?=wc_get_cart_url();?>"><?php displaySvg('cart-icon.svg');?> <span><span class="text"><?=__('Panier', 'wpb');?></span> <span class="value">(<?=$woocommerce->cart->cart_contents_count;?>)</span></span></a></li>
				</ul>
			</nav>
		</header><!-- .site-header -->
	</div>
	<div id="content-full">
		<div id="content" class="site-content">
	

<?php get_header();
global $blogpage_id, $wp_query;
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<div class="builder">
						<?= apply_filters( 'the_content', get_post_field( 'post_content', $blogpage_id ) ); ?>
						<div class="page-item filters">
							<div class="container">
								<a href="<?= get_permalink($blogpage_id); ?>" class="animate to-bottom fade btn link <?php if(is_home()): ?>current<?php endif;?>"><?php _e('All posts', 'wpb'); ?></a>
								<?php
								$taxonomy = 'category';
								$taxonomy_terms = get_terms( $taxonomy, array(
								    'hide_empty' => 0,
								    'orderby' => 'term_order',
								) );
								$delay = 1.5;
								foreach ($taxonomy_terms as $pcat):
								?>
									<a href="<?= get_term_link($pcat->term_id); ?>" class="animate to-bottom fade <?php animationDisplayDelay($delay); ?> btn link <?php if(get_queried_object()->term_id == $pcat->term_id): ?>current<?php endif;?>"><?= $pcat->name; ?></a>
								<?php 
								    animationIncreaseDelay($delay);
								endforeach;?>
							</div>
						</div>
						<?php
						if ( have_posts() ) :
						$delay = 1;
						?>
							<div class="page-item news">
								<div class="container">
    								<div class="list-container blocks-display-column column-count-4 animate-childrens" id="ajax-target">
        	            				<?php
        	            					while ( have_posts() ) : the_post();
        	            						get_template_part( 'item-list', 'news', array('delay' => $delay) );
        	            						animationIncreaseDelay($delay);
        	            					endwhile;
        	            				?>
    	            				</div>
	            				</div>
	        				</div>
	        				<div class="page-item paginate infinite"><?php
	        				$args = array(
	        						'posts_per_page' => get_field('ppp','config-posts'),
	        						'post_status' => 'publish'
	        				);
	        				displayPagination('', $args, 'news'); ?></div>
						<?php
						endif;
						?>
					</div>
				</div>
			</article>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>

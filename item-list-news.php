<?php global $blogpage_id, $cid; 
if(!isset($args)): $args = array('delay' => 1.5); endif;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('item animate fade to-right '.animationDisplayDelay($args['delay'], false)); ?>>
	<div class="image">
		<a class="ir ir-3-2" href="<?= get_the_permalink(); ?>"><?=getImage('portrait_@1x');  ?></a>
		<div class="label">
    		<?= get_the_category_list(); ?>
    	</div>
	</div>
	<div class="text">
 		<h3><a href="<?= get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
	</div>
</article>

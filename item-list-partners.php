<?php 
$website = get_field('website', get_the_ID());
if($website):
    $website = addScheme($website);
endif;
if(!isset($args)): $args = array('delay' => 1.5); endif;
?>
<div class="item animate fade to-right <?php animationDisplayDelay($args['delay']); ?>">
	<div class="image">
	<?php if($website): ?><a href="<?= $website; ?>" target="_blank" rel="noopener"><?php endif;?>
		<?php the_post_thumbnail('rectangle_nc'); ?>
	<?php if($website): ?></a><?php endif;?>
	</div>
	<div class="text">
		<h3>
			<?php if($website): ?><a href="<?= $website; ?>" target="_blank" rel="noopener"><?php endif;?>
				<?php the_title(); ?>
			<?php if($website): ?></a><?php endif;?>
		</h3>
		<div class="description"><?php the_field('description', get_the_ID()); ?></div>
	    <?php if($website): ?>
	    	<a href="<?= $website; ?>" class="btn link" target="_blank" rel="noopener"><?= __('Visit', 'wpb'); displaySvg('arrow-right-4.svg'); ?></a>
	    <?php endif; ?>
    </div>
</div>
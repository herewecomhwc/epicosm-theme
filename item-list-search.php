<article id="post-<?php the_ID(); ?>" <?php post_class('item'); ?>>
	<?php if(has_post_thumbnail()): ?>
    	<div class="image">
    		<a href="<?= get_the_permalink(); ?>"><?php the_post_thumbnail('rectangle_@1x');  ?></a>
    	</div>
	<?php endif; ?>
	<div class="text">
 		<h3><a href="<?= get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="description"><?php the_field('resume', get_the_ID()); ?></div>
		<a href="<?= get_the_permalink(); ?>" class="btn link" title="<?php the_title(); ?>"><?php the_field('btn_post_read_more', 'config-content-'.pll_current_language()); displaySvg('arrow-right-4.svg'); ?></a>
	</div>
</article>

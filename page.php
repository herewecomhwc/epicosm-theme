<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() && !is_404() ) :
			while ( have_posts() ) : the_post();
    			if ( post_password_required() ) :
    			    $post = get_post($protected_id);
        			setup_postdata($post);
        			$cid = $protected_id;
    			endif;
    			get_template_part( 'content', 'page' );
			endwhile;
		else:
			// 404
			$post = get_post($cid);
			setup_postdata($post);
			get_template_part( 'content', 'page' );
		endif;

		?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
<?php
$url_form_mailchimp = get_field('url_form_mailchimp', 'config-hwc');
if(!empty($url_form_mailchimp)) : ?>
	<!-- MAILCHIMP FORM  -->
	<form action="<?= $url_form_mailchimp; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		<div id="mc_embed_signup_scroll">
			<div class="mc-field-group fields-container items-1">
				<div class="field">
    				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="<?php _e('Your email address', 'wpb'); ?>">
    				<input type="submit" value="<?=get_field('label_subscribe_btn', 'config-content-'.pll_current_language()); ?>" class="btn default" name="subscribe" id="mc-embedded-subscribe">
				</div>
			</div>
			<div class="rgpd fields-container acceptance mc-field-group">
				<div class="item">

				</div>
				<label for="rgpd">
					<span class="wpcf7-form-control-wrap your-consent">
						<span class="wpcf7-form-control wpcf7-acceptance">
							<span class="wpcf7-list-item">
								<input type="checkbox" name="rgpd" id="rgpd" class="required" required />
							</span>
						</span>
					</span>
					<?=get_field('rgpd_field_description', 'config-content-'.pll_current_language());?>
				</label>
			</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display: none"></div>
				<div class="response" id="mce-success-response" style="display: none"></div>
			</div>
			<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1a9dc4b847d0f8b1485e72e15_b31fb96800" tabindex="-1" value=""></div>
		</div>
	</form>
	<!--  ! MAILCHIMP FORM ! -->

	<!-- MAILCHIMP JS -->

	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));</script>
	<!-- ! MAILCHIMP JS ! -->

	<?php $cur_l = pll_current_language(); ?>
	<script type="text/javascript">
	var $mcj = jQuery.noConflict(true);
	$mcj.extend($mcj.validator.messages, {
		<?php
		switch ($cur_l):
		    case 'fr': // FRENCH ?>
				required: "Ce champ est obligatoire.",
				remote: "Veuillez corriger ce champ.",
				email: "Veuillez fournir une adresse électronique valide.",
				url: "Veuillez fournir une adresse URL valide.",
				date: "Veuillez fournir une date valide.",
				dateISO: "Veuillez fournir une date valide (ISO).",
				number: "Veuillez fournir un numéro valide.",
				digits: "Veuillez fournir seulement des chiffres.",
				creditcard: "Veuillez fournir un numéro de carte de crédit valide.",
				equalTo: "Veuillez fournir encore la même valeur.",
				extension: "Veuillez fournir une valeur avec une extension valide.",
				maxlength: $mcj.validator.format( "Veuillez fournir au plus {0} caractères." ),
				minlength: $mcj.validator.format( "Veuillez fournir au moins {0} caractères." ),
				rangelength: $mcj.validator.format( "Veuillez fournir une valeur qui contient entre {0} et {1} caractères." ),
				range: $mcj.validator.format( "Veuillez fournir une valeur entre {0} et {1}." ),
				max: $mcj.validator.format( "Veuillez fournir une valeur inférieure ou égale à {0}." ),
				min: $mcj.validator.format( "Veuillez fournir une valeur supérieure ou égale à {0}." ),
				maxWords: $mcj.validator.format( "Veuillez fournir au plus {0} mots." ),
				minWords: $mcj.validator.format( "Veuillez fournir au moins {0} mots." ),
				rangeWords: $mcj.validator.format( "Veuillez fournir entre {0} et {1} mots." ),
				letterswithbasicpunc: "Veuillez fournir seulement des lettres et des signes de ponctuation.",
				alphanumeric: "Veuillez fournir seulement des lettres, nombres, espaces et soulignages.",
				lettersonly: "Veuillez fournir seulement des lettres.",
				nowhitespace: "Veuillez ne pas inscrire d'espaces blancs.",
				ziprange: "Veuillez fournir un code postal entre 902xx-xxxx et 905-xx-xxxx.",
				integer: "Veuillez fournir un nombre non décimal qui est positif ou négatif.",
				vinUS: "Veuillez fournir un numéro d'identification du véhicule (VIN).",
				dateITA: "Veuillez fournir une date valide.",
				time: "Veuillez fournir une heure valide entre 00:00 et 23:59.",
				phoneUS: "Veuillez fournir un numéro de téléphone valide.",
				phoneUK: "Veuillez fournir un numéro de téléphone valide.",
				mobileUK: "Veuillez fournir un numéro de téléphone mobile valide.",
				strippedminlength: $mcj.validator.format( "Veuillez fournir au moins {0} caractères." ),
				email2: "Veuillez fournir une adresse électronique valide.",
				url2: "Veuillez fournir une adresse URL valide.",
				creditcardtypes: "Veuillez fournir un numéro de carte de crédit valide.",
				ipv4: "Veuillez fournir une adresse IP v4 valide.",
				ipv6: "Veuillez fournir une adresse IP v6 valide.",
				require_from_group: "Veuillez fournir au moins {0} de ces champs.",
				nifES: "Veuillez fournir un numéro NIF valide.",
				nieES: "Veuillez fournir un numéro NIE valide.",
				cifES: "Veuillez fournir un numéro CIF valide.",
				postalCodeCA: "Veuillez fournir un code postal valide."
		<?php
			break;
		?>
		<?php
			case 'it': // ITALIAN
		?>
				required: "Campo obbligatorio",
				remote: "Controlla questo campo",
				email: "Inserisci un indirizzo email valido",
				url: "Inserisci un indirizzo web valido",
				date: "Inserisci una data valida",
				dateISO: "Inserisci una data valida (ISO)",
				number: "Inserisci un numero valido",
				digits: "Inserisci solo numeri",
				creditcard: "Inserisci un numero di carta di credito valido",
				equalTo: "Il valore non corrisponde",
				extension: "Inserisci un valore con un&apos;estensione valida",
				maxlength: $mcj.validator.format( "Non inserire pi&ugrave; di {0} caratteri" ),
				minlength: $mcj.validator.format( "Inserisci almeno {0} caratteri" ),
				rangelength: $mcj.validator.format( "Inserisci un valore compreso tra {0} e {1} caratteri" ),
				range: $mcj.validator.format( "Inserisci un valore compreso tra {0} e {1}" ),
				max: $mcj.validator.format( "Inserisci un valore minore o uguale a {0}" ),
				min: $mcj.validator.format( "Inserisci un valore maggiore o uguale a {0}" ),
				nifES: "Inserisci un NIF valido",
				nieES: "Inserisci un NIE valido",
				cifES: "Inserisci un CIF valido",
				currency: "Inserisci una valuta valida"
		<?php
			break;
		?>
		<?php
			case 'es': // SPANISH
		?>
				required: "Este campo es obligatorio.",
				remote: "Por favor, rellena este campo.",
				email: "Por favor, escribe una dirección de correo válida.",
				url: "Por favor, escribe una URL válida.",
				date: "Por favor, escribe una fecha válida.",
				dateISO: "Por favor, escribe una fecha (ISO) válida.",
				number: "Por favor, escribe un número válido.",
				digits: "Por favor, escribe sólo dígitos.",
				creditcard: "Por favor, escribe un número de tarjeta válido.",
				equalTo: "Por favor, escribe el mismo valor de nuevo.",
				extension: "Por favor, escribe un valor con una extensión aceptada.",
				maxlength: $mcj.validator.format( "Por favor, no escribas más de {0} caracteres." ),
				minlength: $mcj.validator.format( "Por favor, no escribas menos de {0} caracteres." ),
				rangelength: $mcj.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
				range: $mcj.validator.format( "Por favor, escribe un valor entre {0} y {1}." ),
				max: $mcj.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
				min: $mcj.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
				nifES: "Por favor, escribe un NIF válido.",
				nieES: "Por favor, escribe un NIE válido.",
				cifES: "Por favor, escribe un CIF válido."
		<?php
			break;
		?>
		<?php
			case 'de': // DEUTSH
		?>
				required: "Dieses Feld ist ein Pflichtfeld.",
				maxlength: $mcj.validator.format( "Geben Sie bitte maximal {0} Zeichen ein." ),
				minlength: $mcj.validator.format( "Geben Sie bitte mindestens {0} Zeichen ein." ),
				rangelength: $mcj.validator.format( "Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein." ),
				email: "Geben Sie bitte eine gültige E-Mail Adresse ein.",
				url: "Geben Sie bitte eine gültige URL ein.",
				date: "Bitte geben Sie ein gültiges Datum ein.",
				number: "Geben Sie bitte eine Nummer ein.",
				digits: "Geben Sie bitte nur Ziffern ein.",
				equalTo: "Bitte denselben Wert wiederholen.",
				range: $mcj.validator.format( "Geben Sie bitte einen Wert zwischen {0} und {1} ein." ),
				max: $mcj.validator.format( "Geben Sie bitte einen Wert kleiner oder gleich {0} ein." ),
				min: $mcj.validator.format( "Geben Sie bitte einen Wert größer oder gleich {0} ein." ),
				creditcard: "Geben Sie bitte eine gültige Kreditkarten-Nummer ein."
		<?php
			break;
		?>
		<?php
			case 'ru': // RUSSE
		?>
				required: "Это поле необходимо заполнить.",
				remote: "Пожалуйста, введите правильное значение.",
				email: "Пожалуйста, введите корректный адрес электронной почты.",
				url: "Пожалуйста, введите корректный URL.",
				date: "Пожалуйста, введите корректную дату.",
				dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
				number: "Пожалуйста, введите число.",
				digits: "Пожалуйста, вводите только цифры.",
				creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
				equalTo: "Пожалуйста, введите такое же значение ещё раз.",
				extension: "Пожалуйста, выберите файл с правильным расширением.",
				maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
				minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
				rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
				range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
				max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
				min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
		<?php
			break;
			endswitch;
		?>
	});
	</script>
<?php endif; ?>
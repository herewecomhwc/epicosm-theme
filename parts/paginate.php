<span class="maxpage" attr-ajax-value="<?=$html_params['maxpages'];?>"></span>
<span class="template" attr-ajax-value="<?=$html_params['template'];?>"></span>
<span class="page" attr-ajax-value="<?=$html_params['paged']+1;?>"></span>
<div class="message text-color" style="display:none;"><i class="fas fa-spinner fa-pulse"></i> <?= __('Chargement ...', 'wpb'); ?></div>
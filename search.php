<?php 
get_header(); 
global $cid;
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article id="post-<?= $cid; ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<div class="builder">
    					<?php 
    					$p_object = get_post( $cid );
    					echo apply_filters('the_content', $p_object->post_content);
    					?>
    					<div class="page-item form">
    						<div class="container">
    							<?php get_search_form(); ?>
    						</div>
    					</div>
    					<div class="page-item list">
        					<div class="container list-container blocks-display-inline">
            					<?php
            					if ( have_posts() ) : 
            						while ( have_posts() ) : the_post();
            							get_template_part( 'item-list', 'search' ); 
            						endwhile;
            					endif;
            					?>
        					</div>
    					</div>
					</div>
				</div>
			</article>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
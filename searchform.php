<form role="search" method="get" class="search-form" action="<?= home_url( '/' ); ?>">
    <label>
        <input type="search" class="search-field"
            placeholder="<?= esc_attr_x( 'Search …', 'placeholder', 'wpb'  ) ?>"
            value="<?= get_search_query() ?>" name="s"
            title="<?= esc_attr_x( 'Search for:', 'label', 'wpb'  ) ?>" />
    </label>
    <input type="submit" class="search-submit btn default"
        value="<?= esc_attr_x( 'Search', 'submit button', 'wpb'  ) ?>" />
</form>
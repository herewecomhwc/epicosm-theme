<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();

    			if ( post_password_required() ) :
    			    $post = get_post($protected_id);
        			setup_postdata($post);
        			get_template_part( 'content-single', 'post');
    			else:
        			if(hasPostTypeGutenbergDisabled(get_post_type())):
        			    get_template_part( 'content-single', 'no-gutenberg');
        			else:
        			    get_template_part( 'content-single', get_post_type() );
        			endif;
    			endif;
			endwhile;
		endif;
		?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
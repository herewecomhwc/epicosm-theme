<?php defined('ABSPATH') || die; ?>

<div class="fields-container items-1" data-field="{{ field_name }}">
	<div class="field">
        {{ label }}
        {{ field }}
        {{ errors }}
    </div>
</div>

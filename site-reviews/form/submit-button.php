<?php defined('ABSPATH') || die; ?>

<div data-field="submit-button">
    <button type="submit" class="btn default" data-loading="{{ text }}" aria-busy="false">
        <span class="glsr-button-loading"></span>
        <span class="glsr-button-text">{{ text }}</span>
    </button>
</div>

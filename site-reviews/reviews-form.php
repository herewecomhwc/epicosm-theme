<?php defined('ABSPATH') || die; ?>

<div class="glsr-form-wrap">
    <form class="form" method="post" enctype="multipart/form-data">
        {{ fields }}
        {{ response }}
        {{ submit_button }}
    </form>
</div>

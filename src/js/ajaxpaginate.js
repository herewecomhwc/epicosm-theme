jQuery( document ).ready(function($) {
	if($('.page-item.paginate.infinite').length > 0 && $('#ajax-target').length > 0)
	{
		paginateHandle();
		$(window).trigger('scroll');
	}
	 
	function paginateHandle()
	{
		var process = false;
		$( window ).scroll(function() {
			var bottomPos = $(window).scrollTop() + $(window).height();
			var bottomPaginatePost = $(".page-item.paginate").offset().top;
			var curpage = $('.page-item.paginate .page').attr('attr-ajax-value');
			var maxpage = $('.page-item.paginate .maxpage').attr('attr-ajax-value');
			if(bottomPaginatePost <= bottomPos && parseInt(curpage) <= parseInt(maxpage) && !process)
			{
				process = true;
				$('.page-item.paginate > .paginate').css('display', 'none');
				$.ajax({
					url: ajaxpagination.ajaxurl,
					type: 'post',
					data: {
						action: 'ajax_pagination',
						page: $('.page-item.paginate .page').attr('attr-ajax-value'),
						template: $('.page-item.paginate .template').attr('attr-ajax-value'),
						args: ajaxargs,
					}, 
					beforeSend: function() {
						$('.page-item.pagination .message').css('display', 'block');
					},
					success: function( html ) {
						$('.page-item.pagination .message').css('display', 'none');
						var page = parseInt($('.page-item.paginate .page').attr('attr-ajax-value'));
						page++;
						$('.page-item.paginate .page').attr('attr-ajax-value', page);
						$('#ajax-target').append( html );
						process = false;
						$(".animate").appear(function(){
							$(this).addClass("animated");
						});
						$(".list-container .item").appear(function(){
							$(this).addClass("animated");
						});
						$(".animate-childrens").appear(function(){
							$(this).children().addClass("animated");
						});
					}
				})
			}
			else {
				$('.page-item.paginate .message').css('display', 'none');
			}
		});
	}
});
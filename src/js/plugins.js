jQuery(document).ready(function($){
	
	scrollManager();
	popinInit();
	slidersInit();

function scrollManager() {
	$('.page-item.numbers').appear(function(){
		var delay = 0;
		$(this).find(".line-1").each(function() {
			var $i = $(this);
			var gt = parseInt($i.text());
			$i.text(0);
			setTimeout(function() {
				$i.countTo({
					from: 0,
					to: gt,
					speed: 2000,
					refreshInterval: 50,
					onComplete: function(value) {}
				});
			}, delay);
			delay += 300;
		});
	});
	$(".page-item").appear(function(){
		$(this).addClass("appear");
		removeAnimationClasses($(this));
	});
	$(".animate").appear(function(){
		$(this).addClass("animated");
		removeAnimationClasses($(this));
	});
	$(".animate-childrens").appear(function(){
		$(this).find('.animate').addClass("animated");
		removeAnimationClasses($(this).find('.animate'));
	});
	$(window).trigger("scroll");
	
	// On supprime les classes delay-xxx pour éviter d'avoir des secousses quand on retaille la fenêtre

	function removeAnimationClasses(item) {
		setTimeout(
		  function() 
		  {
			  item.removeClass (function (index, className) {
			    return (className.match (/(^|\s)delay-\S+/g) || []).join(' ');
			});
			  item.removeClass ('animate');
		  }, 5000);
	}
}

function slidersInit() {
	$("[data-slider-action]").on("click", function(){
		var data_arr = $(this).data('slider-action');
		var slider = data_arr[0];
		var goto = slider.goto;
		var speed = 1000;
		if(slider.speed !== undefined) {
			speed = slider.speed;
		}
		var id = slider.id;
		var owl = $(id).owlCarousel();
		
		if(goto == 'prev') {
			owl.trigger('prev.owl.carousel', [speed]);
		}
		else if(goto == 'next') {
			owl.trigger('next.owl.carousel', [speed]);
		}
		else if(Math.floor(goto) == goto && $.isNumeric(goto )) {
			owl.trigger('to.owl.carousel', [goto, speed]);
		}
	});
	/* 
	Ex :  data-slider-init='[{"items":"2", "navText":["Précédent","Suivant"]}]'
	Il est possible de passer n'importe quel attribut de OwlCarousel : https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html
	*/
	$("[data-slider-init]").each(function(){
		$(this).addClass('owl-carousel');
		var owl_options = {
			autoplay: true,
			autoplayHoverPause: true,
			autoplaySpeed: 1000,
			dots: false,
			items: 1,
			stagePadding : 0,
			loop: true,
			center: false,
			mouseDrag: true,
			margin: 0,
			nav: true,
			navSpeed: 1500,
			navText: ['', ''],
	
		};
		var data_arr = $(this).data('slider-init');
		$.each(data_arr, function(index, value){		
			var key = Object.entries(value)[0][0];
			var val = Object.entries(value)[0][1];
			if(val == "true") {
				val = true;	
			}
			else if(val == "false") {
				val = false;
			}
			if(key == 'margin') {
				val = parseInt(val);
			}
			
			owl_options[key] = val;
		});
		$(this).owlCarousel(owl_options);
	})
	
	var owl_partners = $(".owl-partners").owlCarousel({
		autoplay: true,
		autoplayHoverPause: true,
		autoplaySpeed: 1500,
		dots: false,
		items: 6,
		margin: 30,
		loop: true,
		mouseDrag: false,
		nav: false,
		navSpeed: 1500,
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 2
			},
			600: {
				items: 3
			},
			800: {
				items: 4
			},
			1000: {
				items: 5
			},
			1200: {
				items: 6
			}
		}
	});
	
	var owl_testis_options = {
		autoplay: false,
		autoplayHoverPause: false,
		autoplaySpeed: 1500,
		dots: true,
		items: 3,
		stagePadding : -120,
		loop: true,
		center: true,
		mouseDrag: true,
		margin: 5,
		nav: true,
		navSpeed: 1500,
		navText: ['', ''],

	};
	var testis_count = 1;
	var stage_0;
	var stage_1;
	$(".owl-testimonials").each(function(){
		testis_count = $(this).find('.item').length;
		if(testis_count >= 4) {
			owl_testis_options['responsive'] = 
			{
				0: {
					items: 1,
					stagePadding : 0,
					margin: 0
				},
				850: {
					items: 2,
					stagePadding : 0,
					
				},
				1025: {
					items: 3,
				},
				1600: {
					items: 4
				}
			}
			$(this).owlCarousel(owl_testis_options);
		}
		else {
			stage_0 = testis_count - 2; if(stage_0 < 1) {stage_0 = 1;}
			stage_1 = testis_count - 1; if(stage_1 < 1) {stage_1 = 1;}
			$(this).addClass('no-loop');
			owl_testis_options['items'] = testis_count;
			owl_testis_options['stagePadding'] = 0;
			owl_testis_options['loop'] = false;
			owl_testis_options['center'] = false;
			owl_testis_options['autoWidth'] = true;
	
			owl_testis_options['responsive'] = 
			{
				0: {
					items: stage_0,
					stagePadding : 0,
					margin: 0,
				},
				850: {
					items: stage_1,
					stagePadding : 0,
					
				},
				1025: {
					items: testis_count,
				}
			}
			$(this).owlCarousel(owl_testis_options);
		}
	})
	
}

function popinInit() {
	$(".page-item.gallery, .page-item.alternate-blocks .images, .wp-gallery").each(function() {
		$(this).magnificPopup({
			delegate: "a.popin",
			type: "image", 
			gallery: {
				enabled: true
			}
		});
	});
	$(".woocommerce-product-gallery__wrapper").each(function() {
		$(this).magnificPopup({
			delegate: ".woocommerce-product-gallery__image > a",
			type: "image", 
			gallery: {
				enabled: true
			}
		});
	});
	
	$('.open-mailchimp-popup').magnificPopup({
	  type:'inline',
	  midClick: true,
	  closeBtnInside : true,
	  closeMarkup : '<button title="%title%" type="button" class="mfp-close"></button>',
	});
}
 
});
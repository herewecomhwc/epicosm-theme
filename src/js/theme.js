jQuery(document).ready(function($){
	
	videoManager();
	drawersManager();
	scrollManager();
	window.setTimeout( emailDisplayInit(), 1000 );
	otherThings();
	svgDisplay();
	classesManager();
	woocommerceManager();
	menuManager();
	
	// Wait 2 seconds max if body not loaded
	setTimeout(function() {
		$("#loading-screen").addClass("loaded");
   }, 2000);
	
	$(window).on('load', function() {
		$("#loading-screen").addClass("loaded");
	})
	
/*
Permet d'enregistrer facilement des evenements onclick pour ajouter / supprimer des classes sur des elements
Exemples d'utilisation : 
<div data-onclick='[{"class":"menu-open","action":"toggleClass","target":"#target, html, body"}]'></div> On toggle la classe menu-open sur #target, html et body
<div data-onclick='[{"class":"active","action":"addClass","remove":"#target2","target":"#target"}]'></div> On ajoute la classe active sur #target et on l'enleve sur #target2
<div data-onclick='[{"class":"active","action":"removeClass","target":"#target"}]'></div> On supprime la classe active sur #target
<div data-onclick='[{"class":"active1","action":"removeClass","target":"#target1"}, {"class":"active2","action":"removeClass","target":"#target2"}]'></div> On supprime la classe active1 sur #target1 et la classe active2 sur #target2
*/		
function classesManager() {
	$("[data-onclick]").on("click", function(){
		var data_arr = $(this).data('onclick');
		$.each(data_arr, function(index, value){
			var onclick = data_arr[index];
			var classes = onclick.class;
			var action = onclick.action;
			var target = onclick.target;
			if(action == 'addClass') {
				if(onclick.hasOwnProperty('remove')) {
					$(onclick.remove).removeClass(classes);
				}
				$(target).addClass(classes);			
			}
			else if(action == 'removeClass') {
				$(target).removeClass(classes);
			}
			else if(action == 'toggleClass') {
				$(target).toggleClass(classes);
			}
		})
	});
}

function menuManager() {
	$("#main-menu ul.menu > .menu-item-has-children > a, #main-menu ul.menu > li li.list > a").on("click", function(e){
		e.preventDefault();
		if($(this).parent().hasClass('open')) {
			$(this).parent().removeClass('open');
			if(!$(this).parent().hasClass('list')) {
				$("#main-menu ul.menu li:not(.open)").removeClass('close');
			}
			else {
				$(this).parent().addClass('close');
			}
		}
		else {
			if(!$(this).parent().hasClass('list')) {
				$("#main-menu ul.menu li").removeClass('open');
			}
			$(this).parent().addClass('open').removeClass('close');
			$("#main-menu ul.menu li:not(.open)").addClass('close');
		}
		
	});
}
	
// On remplace les balises IMG chargeant des svg par leur code complet
function svgDisplay() {
	$('img[src$=".svg"]').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}

function scrollManager() {
	$(window).scroll(function(){
		// On scroll
		if($(window).scrollTop() >= 100) {
			$('body').addClass('fixed');
		}
		else {
			$('body').removeClass("fixed");
		}
	});
	$(window).trigger("scroll");
	
	// Smooth scroll au clic sur une ancre
	var $root = $('html, body');
	$(document).on('click', 'a[href^="#"]:not([href="#"])', function (event) {
	    event.preventDefault();
	    $root.animate({
	        scrollTop: $($.attr(this, 'href')).offset().top //- $('#header-container').height()
	    }, 1000);
	});
	// Smooth scroll au chargement de la page si une ancre est présent dans l'url
	if(window.location.hash && window.location.hash.length > 0) {
	  var hash = window.location.hash;
	setTimeout(function() {
	  $root.animate({
		    scrollTop: $(hash).offset().top //- $('#header-container').height()
		  }, 1000);
	}, 100);
	}
}

function videoManager() {
	$(".page-item.video .hover").on("click", function(e){
		e.preventDefault();
		var $i = $(this);
		$i.parent().find('iframe')[0].src += "?rel=0&autoplay=1";
		$i.addClass("hidden");
		$i.prev().addClass("played");
		$i.nextAll('.play-content').first().addClass('hidden');
	});
	
}

function drawersManager() {

	$(".page-item.drawers .item:not(.open) .content").fadeOut(0);
	$(".page-item.drawers .head").on("click", function(){
		var item = $(this).parent();
		item.toggleClass('open');
		item.find('.content').slideToggle();
	});
}

function emailDisplayInit() {
	$(".email-js").each(function() {
		var $i = $(this);
		var href = $i.attr("href"); 
		var html = $i.html();
		var text = $i.text(); 
		if(href || html || text) {
			if(href) {
				href = href.replace(/\@no-spam-/g,'@');
				$i.attr("href", href);
			}
			if(html) {
				html = html.replace(/\@no-spam-/g,'@');
				$i.html(html);
			}
			else if(text) {
				text = text.replace(/\@no-spam-/g,'@');
				$i.text(text);
			}
		}
	});
}

function otherThings() {
	$("#main iframe:not(.no-video-container)").wrap("<div class=\"video-container\"></div>");
	$("#main li").wrapInner("<span />");
	$("<span class='render-radio-checkbox'></span>").insertAfter( [
		".wpcf7-checkbox input[type=checkbox]", 
		".wpcf7-acceptance input[type=checkbox]", 
		".wpcf7-radio input[type=radio]"
	] );
	$(".wpcf7-form [placeholder]").each(function() {
		$("<span class='placeholder'>"+$(this).attr('placeholder')+"</span>").insertAfter( $(this));
	});
	
}

function woocommerceManager() {
		if($('.woocommerce-tabs').length > 0) {
			$('.woocommerce-tabs .panel').addClass('close');
			$('.woocommerce-tabs').on( 'click', '.panel .block-title', function() {
				$(this).parent().toggleClass('close');
			});
		}
		
        $('body').on( 'click', 'form.cart button.plus, button.minus', function() {
            // Get current quantity values
            var qty = $( this ).closest( '.qty-container' ).find( '.qty' );
            var val   = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
 
            // Change the value if plus or minus
            if ( $( this ).is( '.plus' ) ) {
               if ( max && ( max <= val ) ) {
                  qty.val( max );
               } 
            else {
               qty.val( val + step );
                 }
            } 
            else {
               if ( min && ( min >= val ) ) {
                  qty.val( min );
               } 
               else if ( val > 1 ) {
                  qty.val( val - step );
               }
            }
            qty.trigger('change');
             
         });
}

});
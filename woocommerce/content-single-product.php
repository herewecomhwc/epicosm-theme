<?php 
global $cid, $product; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="builder">
			<div class="page-item text">
				<div class="container woocommerce">
					<?php do_action( 'woocommerce_before_single_product' );

                    if ( post_password_required() ) {
                    	echo get_the_password_form(); // WPCS: XSS ok.
                    	return;
                    }
                    ?>
                    <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
                		<?php 
                        $float_btn = get_field('dealer_button', 'config-content-'.pll_current_language());
                        if($float_btn): ?>
                        	<a href="<?=$float_btn['url'];?>" class="btn default vertical" target="<?=$float_btn['target'];?>"><?=$float_btn['title'];?></a>
						<?php endif; ?>
                    	<?php
                    	/**
                    	 * Hook: woocommerce_before_single_product_summary.
                    	 *
                    	 * @hooked woocommerce_show_product_sale_flash - 10
                    	 * @hooked woocommerce_show_product_images - 20
                    	 */
                    	do_action( 'woocommerce_before_single_product_summary' );
                    	?>
                    
                    	<div class="summary entry-summary">
                    		<div class="container">
                        		<?php
                        		/**
                        		 * Hook: woocommerce_single_product_summary.
                        		 *
                        		 * @hooked woocommerce_template_single_title - 5
                        		 * @hooked woocommerce_template_single_rating - 10
                        		 * @hooked woocommerce_template_single_price - 10
                        		 * @hooked woocommerce_template_single_excerpt - 20
                        		 * @hooked woocommerce_template_single_add_to_cart - 30
                        		 * @hooked woocommerce_template_single_meta - 40
                        		 * @hooked woocommerce_template_single_sharing - 50
                        		 * @hooked WC_Structured_Data::generate_product_data() - 60
                        		 */
                        		do_action( 'woocommerce_single_product_summary' );
                        		$featured_datas = get_field('featured_datas');
                        		if($featured_datas):
                        		?>
                        		<div class="featured_datas">
                        			<?php foreach($featured_datas as $featured_data): ?>
                        				<div class="data">
                        					<div class="img"><?=getImage('rectangle_nc_@0.5x', $featured_data['icon']['ID']); ?></div>
        									<div class="text"><?=$featured_data['text'];?></div>
                        				</div>
                        			<?php endforeach; ?>
                        		</div>
                        		<?php endif; ?>
                    		</div>
                    	</div>
                    
                    	<?php
                    	/**
                    	 * Hook: woocommerce_after_single_product_summary.
                    	 *
                    	 * @hooked woocommerce_output_product_data_tabs - 10
                    	 * @hooked woocommerce_upsell_display - 15
                    	 * @hooked woocommerce_output_related_products - 20
                    	 */
                    	do_action( 'woocommerce_after_single_product_summary' );
                    	?>
                    </div>
                    
                    <?php do_action( 'woocommerce_after_single_product' ); ?>
				</div>
			</div>
		</div>
	</div>
</article>

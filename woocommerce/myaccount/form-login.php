<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="page-item woocommerce-form-login">
	<div class="container">
        <?php 
        do_action( 'woocommerce_before_customer_login_form' ); ?>
        
        <?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
        
        <div class="u-columns col2-set" id="customer_login">
        
        	<div class="u-column1 col-1">
        
        <?php endif; ?>
        
        		<h2><?php esc_html_e( 'Déjà client·e', 'woocommerce' ); ?></h2>
        
        		<form class="woocommerce-form woocommerce-form-login login form" method="post">
        
        			<?php do_action( 'woocommerce_login_form_start' ); ?>
        
        			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        				<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        				<input type="text" name="username" id="username" placeholder="<?=__('email*', 'wpb'); ?>" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
        				<span class="placeholder"><?=__('email*', 'wpb'); ?></span>
        			</p>
        			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        				<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        				<input type="password" placeholder="<?=__('mot de passe*', 'wpb'); ?>" name="password" id="password" />
        				<span class="placeholder"><?=__('mot de passe*', 'wpb'); ?></span>
        			</p>
        
        			<?php do_action( 'woocommerce_login_form' ); ?>
        
        			<p class="form-row">
        				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
        					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
        				</label>
        				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
        				<button type="submit" class="woocommerce-button woocommerce-form-login__submit btn default" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
        			</p>
        			<p class="woocommerce-LostPassword lost_password">
        				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="btn link"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
        			</p>
        
        			<?php do_action( 'woocommerce_login_form_end' ); ?>
        
        		</form>
        
        <?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
        
        	</div>
        
        	<div class="u-column2 col-2">
        
        		<h2><?php esc_html_e( 'Pas encore client·e', 'woocommerce' ); ?></h2>
        		<button id="toggle-register-form" data-onclick='[{"class":"hidden","action":"addClass","target":"#toggle-register-form, html, body"}, {"class":"open","action":"addClass","target":"form.register.form"}]' class="btn default"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
        		<form method="post" class="woocommerce-form woocommerce-form-register register form" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
        
        			<?php do_action( 'woocommerce_register_form_start' ); ?>
        
        			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
        
        				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        					<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        					<input type="text" name="username" id="reg_username" autocomplete="username" placeholder="<?=__('email*', 'wpb'); ?>" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
        					<span class="placeholder"><?=__('email*', 'wpb'); ?></span>
        				</p>
        
        			<?php endif; ?>
        
        			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        				<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        				<input type="email" name="email" id="reg_email" autocomplete="email" placeholder="<?=__('email*', 'wpb'); ?>" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
        				<span class="placeholder"><?=__('email*', 'wpb'); ?></span>
        			</p>
        
        			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
        
        				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        					<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        					<input type="password" name="password" placeholder="<?=__('mot de passe*', 'wpb'); ?>" id="reg_password" autocomplete="new-password" />
        					<span class="placeholder"><?=__('mot de passe*', 'wpb'); ?></span>
        				</p>
        
        			<?php else : ?>
        
        				<p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
        
        			<?php endif; ?>
        
        			<?php do_action( 'woocommerce_register_form' ); ?>
        
        			<p class="woocommerce-form-row form-row">
        				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
        				<button type="submit" class="btn default" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
        			</p>
        
        			<?php do_action( 'woocommerce_register_form_end' ); ?>
        
        		</form>
        
        	</div>
        
        </div>
        <?php endif; ?>
        
        <?php do_action( 'woocommerce_after_customer_login_form' ); ?>
    </div>
</div>

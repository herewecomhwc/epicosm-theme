<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.3.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! comments_open() ) {
	return;
}


?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<div class="left">
    		<h2 class="woocommerce-Reviews-title">
    			<?=
    			__('Avis clients', 'wpb');
    			?>
    			<?php displaySvg('quote-banner.svg'); ?>
    		</h2>
    		<a href="#review_form" class="btn link"><?=__('Je laisse un avis', 'wpb');?></a>
		</div>
		<?php 
		$reviews = glsr_get_reviews([
		    'assigned_posts' => 'post_id',
		]);
		if ( $reviews ) : ?>
			<ol class="commentlist">
				<?php 
				echo do_shortcode('[site_reviews assigned_posts="post_id" pagination="ajax" display="10"]');
				//wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>
		<?php else : ?>
			<p class="woocommerce-noreviews"><?php esc_html_e( 'There are no reviews yet.', 'woocommerce' ); ?></p>
		<?php endif; ?>
	</div>

		<div id="review_form_wrapper" class="form">
			<div id="review_form">
				<?php
				echo "<h2>".esc_html__( 'Ajouter un avis', 'wpb' )."</h2><div class='form'>". do_shortcode('[site_reviews_form assigned_posts="post_id"]')."</div>";
				?>
			</div>
		</div>

	<div class="clear"></div>
</div>

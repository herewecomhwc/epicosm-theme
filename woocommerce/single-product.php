<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
    			if ( post_password_required() ) :
        			$post = get_post($protected_id);
        			$cid = $protected_id;
        			setup_postdata($post);
        			get_template_part( 'content-page');
    			else:
			     wc_get_template_part( 'content', 'single-product' );
    			endif;
			endwhile;
		endif;
		?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
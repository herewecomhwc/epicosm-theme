<div class="page-item products-slider">
	<div class="container">
    	<div class="block-title">
            <h2 class="animate fade to-right h1"><?=__('Epicosm c’est aussi…');?></h2>
            <div class="description animate fade to-left">
        		<a href="<?=get_permalink(wc_get_page_id( 'shop' ));?>" class="btn link"><?=__('Voir tous nos produits', 'wpb');?></a>
        	</div>
        </div>
        <?php 
        if($related_products): 
            $unique_id = md5(uniqid(rand(), true));
        ?>
        	<div class="slider-container">
                <div class="products-list" id="<?=$unique_id;?>" data-slider-init='[{"dots":"false"}, {"loop":"false"}, {"mouseDrag":"false"}, {"nav":"false"}, {"items":"3"}, {"margin":"100"}, {"responsive":{"1024" : {"items":"3"}, "768" : {"items":"2"}, "0" : {"items":"1"}}}]'>
                	<?php foreach($related_products as $product): 
                	   $wc_product = wc_get_product($product->get_id());
                	   $description_short = get_field('description_short', $product->get_id());
                	   ?>
                    	<div class="product">
                    		<div class="image ir ir-hover ir-3-2">
                    			<a href="<?=get_permalink($product->get_id());?>">
                    				<?=getImage('portrait_@1x', get_post_thumbnail_id($product->get_id())); ?>
                    			</a>
                    		</div>
                    		<div class="text">
                    			<h2 class="title"><a href="<?=get_permalink($product->get_id());?>"><?=get_the_title($product->get_id()); ?></a></h2>
                    			<div class="price"><?=$wc_product->get_price_html();?></div>
                    			<?php if($description_short):?><div class="description-short"><?=$description_short;?></div><?php endif; ?>
                    		</div>
                    	</div>
                	<?php endforeach;?>
                </div>
                <?php if(count($related_products) > 1) :?>
            	<div class="slide-custom-nav<?php if(count($related_products) <= 3):?> no-nav-desktop<?php endif; ?>">
                	<span class="custom-prev" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "prev"}]'><?php displaySvg('arrow-left-4.svg');?></span>
                	<span class="custom-next" data-slider-action='[{"id":"#<?=$unique_id;?>", "goto": "next"}]'><?php displaySvg('arrow-right-4.svg');?></span>
                </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
	</div>
</div>